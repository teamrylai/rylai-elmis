<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormLemburTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::create('form_lembur', function(Blueprint $table) {
            $table->integer('kode')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->time('jam_mulai');
            $table->time('jam_selesai');
            $table->string('keterangan');


        });

        Schema::table('form_lembur', function(Blueprint $table)
        {        	
            $table->foreign('kode')->references('kode')->on('form');
            $table->foreign('project_id')->references('project_id')->on('project')->onDelete('cascade');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('form_lembur', function(Blueprint $table)
        {
            $table->dropForeign('form_lembur_kode_foreign');
        });

        Schema::drop('form_lembur');

        //
	}

}
