<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
        {
            $table->increments('nip');
            $table->string('email');
            $table->string('username');
            $table->string('password', 64);
            $table->string('nama');
            $table->string('jabatan');            
            $table->string('no_telp', 400);
            $table->integer('denda');
            $table->integer('role_id')->unsigned();
            $table->integer('team_id')->unsigned()->nullable();
            $table->string('path');
            $table->string('remember_token', 100)->nullable();
        });

        Schema::table('users', function(Blueprint $table)
        {
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('team_id')->references('team_id')->on('team');
        });

    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

        Schema::table('users', function(Blueprint $table)
        {
            $table->dropForeign('users_role_id_foreign');
            $table->dropForeign('users_team_id_foreign');

        });
        Schema::drop('users');

    }

}
