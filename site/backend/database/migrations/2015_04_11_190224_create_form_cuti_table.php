<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormCutiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::create('form_cuti', function(Blueprint $table) {
            $table->integer('kode')->unsigned();
            $table->integer('jenis_cuti')->unsigned();
            $table->date('tgl_mulai');
            $table->date('tgl_selesai');
            $table->string('alasan');


        });

        Schema::table('form_cuti', function(Blueprint $table)
        {
            $table->foreign('kode')->references('kode')->on('form')->onDelete('cascade');;
            $table->foreign('jenis_cuti')->references('id')->on('jenis_cuti')->onDelete('cascade');;
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('form_cuti', function(Blueprint $table)
        {
            $table->dropForeign('form_cuti_kode_foreign');
            $table->dropForeign('form_cuti_jenis_cuti_foreign');

        });

        Schema::drop('form_cuti');

        //
	}

}
