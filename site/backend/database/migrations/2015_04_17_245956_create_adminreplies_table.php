<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminrepliesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adminreplies', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_penerima')->unsigned(); 
			$table->text('subjek');
			$table->text('isi');
		});

		// Schema::table('adminreplies', function(Blueprint $table)
  //       {

  //           $table->foreign('id_penerima')->references('id_pengirim')->on('pesans');
          
  //       });

        		Schema::table('adminreplies', function(Blueprint $table)
        {

            $table->foreign('id_penerima')->references('nip')->on('users')->onDelete('cascade');;
          
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Schema::table('adminreplies', function(Blueprint $table)
  //       {
  //           //$table->dropForeign('pesans_id_pengirim_foreign');
  //           $table->dropForeign('adminreplies_id_penerima_foreign');

  //       });

        	Schema::table('adminreplies', function(Blueprint $table)
        {
            //$table->dropForeign('pesans_id_pengirim_foreign');
            $table->dropForeign('adminreplies_id_penerima_foreign');

        });
		Schema::drop('adminreplies');
	}

}
