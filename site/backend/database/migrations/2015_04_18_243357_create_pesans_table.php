<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pesans', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_pengirim')->unsigned();
			$table->text('subjek');
			$table->text('isi');
		});

		Schema::table('pesans', function(Blueprint $table)
        {
            $table->foreign('id_pengirim')->references('nip')->on('users')->onDelete('cascade');;
            //user selalu ke admin
            // $table->foreign('id_penerima')->references('nip')->on('users');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pesans', function(Blueprint $table)
        {
            $table->dropForeign('pesans_id_pengirim_foreign');
            // $table->dropForeign('pesans_id_penerima_foreign');

        });
		Schema::drop('pesans');
	}

}
