<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_user', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('nip')->unsigned();
            $table->integer('project_id')->unsigned();

        });

        Schema::table('project_user', function(Blueprint $table)
        {
            $table->foreign('nip')->references('nip')->on('users')->onDelete('cascade');;
            $table->foreign('project_id')->references('project_id')->on('project')->onDelete('cascade');;
        });	

     }																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																							

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('project_user', function(Blueprint $table)
        {
            $table->dropForeign('project_user_nip_foreign');
            $table->dropForeign('project_user_project_id_foreign');

        });


        Schema::drop('project_user');	
    }

}
