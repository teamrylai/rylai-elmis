<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimesheetTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//

        Schema::create('timesheet', function(Blueprint $table) {
            $table->increments('timesheet_id');
            $table->date('tanggal');
            $table->time('waktu_mulai');
            $table->time('waktu_selesai');
            $table->string('kegiatan',60);
            $table->string('status_absensi');
            $table->string('status_isian');
            $table->integer('nip_pekerja')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->timestamps();

        });


        Schema::table('timesheet', function(Blueprint $table)
        {
            $table->foreign('nip_pekerja')->references('nip')->on('users')->onDelete('cascade');;
            $table->foreign('project_id')->references('project_id')->on('project')->onDelete('cascade');;

        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//

        //
        Schema::table('timesheet', function(Blueprint $table)
        {
            $table->dropForeign('timesheet_nip_pekerja_foreign');

        });

        Schema::drop('timesheet');
	}

}
