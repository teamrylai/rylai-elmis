<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::create('form', function(Blueprint $table) {
            $table->increments('kode');
            $table->string('jenis_form');
            $table->string('status');
            $table->timestamps();
            $table->integer('nip_pekerja')->unsigned();

        });

        Schema::table('form', function(Blueprint $table)
        {
            $table->foreign('nip_pekerja')->references('nip')->on('users')->onDelete('cascade');;

        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

        Schema::table('form', function(Blueprint $table)
        {
            $table->dropForeign('form_nip_pekerja_foreign');

        });

        Schema::drop('form');
		//
	}

}
