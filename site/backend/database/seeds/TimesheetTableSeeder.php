<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Timesheet;
// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class TimesheetTableSeeder extends Seeder {

    public function run()
    {
        /*DB::table('timesheet')->delete();

        $begin = new DateTime( '2015-04-01' );
		$end = new DateTime( 'now' ); 

		$interval = new DateInterval('P1D');
		$daterange = new DatePeriod($begin, $interval ,$end);

		foreach( $daterange as $date) { $dates[] = $date->format('Y-m-d'); }

		$nips = ['1','2','3'];
		$status_absensi = ['C', 'H', 'S', 'W', 'D'];
		$waktumulai = '08:00';
		$waktuselesai = '16:00';
		$kegiatan = 'This is random';
		$project_id = ['1', '2', '3'];

		$dateChosen1=[];
		$dateChosen2=[];
		$dateChosen3=[];
		for( $i = 0; $i <= 10 ; $i++ )
		{
			

			foreach( $nips as $nip_pekerja )
			{
				$datepick;
				if($nip_pekerja == '1')
				{
					do {
						$datepick = array_rand($dates, 1);
					}while(array_key_exists($datepick, $dateChosen1));
					$date = $dates[$datepick];
					$dateChosen1[$datepick]= $dates[$datepick];
				}
				if($nip_pekerja == '2')
				{
					do {
						$datepick = array_rand($dates, 1);
					}while(array_key_exists($datepick, $dateChosen2));
					$date = $dates[$datepick];
					$dateChosen2[$datepick]= $dates[$datepick];
				}
				if($nip_pekerja == '3')
				{
					do {
						$datepick = array_rand($dates, 1);
					}while(array_key_exists($datepick, $dateChosen3));
					$date = $dates[$datepick];
					$dateChosen3[$datepick]= $dates[$datepick];
				}
				
				$x = array_rand($status_absensi, 1);
				$status_isian = 'OK';

				$dat = [$date, $end];
				$c = array_rand($dat,1);

				if($status_absensi[$x] == 'C'){
					$status_isian ='C';
				}
				else if($status_absensi[$x] == 'S'){
					$status_isian = 'S';
				}
					
				Timesheet::create([
					'tanggal' => $date,
					'status_absensi' => $status_absensi[$x],
					'waktu_mulai' => $waktumulai,
					'waktu_selesai' => $waktuselesai,
					'kegiatan' => $kegiatan,
					'status_isian' => $status_isian,
					'nip_pekerja' => $nip_pekerja,
					'project_id' => array_rand($project_id,1),
					'created_at' => $dat[$c],
					'updated_at' => $dat[$c]
				]);
			}
			
		}
 */   }

}