<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\JenisCuti;

class JenisCutiTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('jenis_cuti')->delete();

        JenisCuti::create(['jenis_cuti' => 'Annual Leave (Cuti Tahunan)']);
        JenisCuti::create(['jenis_cuti' => 'Additional Leave (Cuti Tambahan)']);
        JenisCuti::create(['jenis_cuti' => 'Unpaid Leave (Cuti Tidak Dibayar / Cuti Potong Gaji)']);
        JenisCuti::create(['jenis_cuti' => 'Paid Personal Leave: Karyawan Menikah']);
        JenisCuti::create(['jenis_cuti' => 'Paid Personal Leave: Anak Menikah']);
        JenisCuti::create(['jenis_cuti' => 'Paid Personal Leave: Kematian Suami/Istri, Orang Tua, Anak, Mertua']);
        JenisCuti::create(['jenis_cuti' => 'Paid Personal Leave: Kematian Saudara']);
        JenisCuti::create(['jenis_cuti' => 'Paid Personal Leave: Istri Melahirkan']);
        JenisCuti::create(['jenis_cuti' => 'Paid Personal Leave: Anak Sunatan']);
        JenisCuti::create(['jenis_cuti' => 'Paid Personal Leave: Pembaptisan']);
        JenisCuti::create(['jenis_cuti' => 'Paid Personal Leave: Cuti Melahirkan']);
        JenisCuti::create(['jenis_cuti' => 'Paid Personal Leave: Ibadah Haji']);
        JenisCuti::create(['jenis_cuti' => 'Paid Personal Leave: Other(Lain-lain)']);
	}

}