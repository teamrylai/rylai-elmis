<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		DB::statement('SET foreign_key_checks = 0');
        DB::statement('SET UNIQUE_CHECKS=0');
		Model::unguard();

		$this->call('RoleTableSeeder');

		/*Model::unguard();
		$this->call('TeamTableSeeder');

		Model::unguard();
		$this->call('UserTableSeeder');

		Model::unguard();
		$this->call('ProjectTableSeeder');

		Model::unguard();
		$this->call('ProjectUserTableSeeder');

		Model::unguard();
		$this->call('JenisCutiTableSeeder');

		Model::unguard();
		$this->call('TimesheetTableSeeder');

		Model::unguard();
		$this->call('PesanTableSeeder');

		Model::unguard();
		$this->call('AdminreplyTableSeeder');
*/

		DB::statement('SET foreign_key_checks = 1');
        DB::statement('SET UNIQUE_CHECKS=1');    
	}

}
