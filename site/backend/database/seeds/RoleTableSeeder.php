<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Role;

class RoleTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('roles')->delete();

        Role::create(['role_name' => 'admin']);
        Role::create(['role_name' => 'team leader']);
        Role::create(['role_name' => 'team member']);
        Role::create(['role_name' => 'non member']);
	}

}
