<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UserTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        /*DB::table('users')->delete();

        User::Create([
                'email' => 'admin@prompt.test',
                'username' => 'CHI LONG QUA',
                'password' => Hash::make('admin'),
                'nama' => 'BananaSlamJamma',
                'jabatan' => 'Undefined',
                'no_telp' => '+62'.rand(80000000000, 89999999999),
                'denda' => '0',
                'role_id' => '1',
                'team_id' => '1',
                'status' => 'approved',
                'path' => 'img/avatar - default.png'
            ]);

        for ($i=1; $i < 10 ; $i++) { 
            
            $role_id = rand(1,4);
            $team_id = rand(1,4);

            User::Create([
                'nip' => $i,
                'email' => 'user'.$i.'@prompt.test',
                'username' => 'user. '.$i,
                'password' => Hash::make($i),
                'nama' => 'Si Nomor '.$i,
                'jabatan' => 'Undefined',
                'no_telp' => '+62'.rand(80000000000, 89999999999),
                'denda' => $i,
                'role_id' => $role_id,
                'team_id' => $team_id,
                'status' => 'approved',
                'path' => 'img/avatar - default.png'
            ]);
        }*/

        //User table akan di seed melalui prompt
        }

}
