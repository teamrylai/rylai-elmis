<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Pesan;

class PesanTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('pesans')->delete();

        // Pesan::create(['id' => '1', 'id_pengirim' => '2','id_penerima' => '3', 'isi' => 'hello world!']);
        // Pesan::create(['id' => '2', 'id_pengirim' => '3','id_penerima' => '2', 'isi' => 'world hello']);
        // Pesan::create(['id' => '3', 'id_pengirim' => '1','id_penerima' => '2', 'isi' => 'zzzz!']);


        Pesan::create(['id' => '1', 'id_pengirim' => '2','subjek' => 'testing', 'isi' => 'hello world!']);
        Pesan::create(['id' => '2', 'id_pengirim' => '3','subjek' => 'nope', 'isi' => 'world hello']);
        Pesan::create(['id' => '3', 'id_pengirim' => '1','subjek' => 'nothing', 'isi' => 'zzzz!']);

	}

}