<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FormCuti extends Model {

	//
    protected $table = 'form_cuti';

    protected $primaryKey = 'kode';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['kode', 'jenis_cuti','tgl_mulai','tgl_selesai','alasan'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    public $timestamps = false;

    public function jeniscuti()
    {
        return $this->hasOne('App\JenisCuti', 'id', 'jenis_cuti');
    }

    public function form()
    {
        return $this->belongsTo('App\Form');
    }
        
        
}
