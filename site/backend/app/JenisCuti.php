<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisCuti extends Model {

	//
	protected $table = 'jenis_cuti';

	protected $primarykey = 'id';

	protected $fillable = ['id', 'jenis_cuti'];

	public $timestamps = false;

}
