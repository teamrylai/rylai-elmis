<?php namespace App\Console;

use App;
use Excel;
use App\User;
use App\Timesheet;
use Mail;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use DateTime;
use DatePeriod;
use DateInterval;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Setting;
use App\SetNotif;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
	'App\Console\Commands\Inspire',
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		/*$setnotif = SetNotif::all();
		$settings = Setting::all();

		if(empty($setnotif)){

		}
		else{
			$schedule->call(function(){

				$users = User::all();
				$today = Carbon::today('Asia/Jakarta')->toDateString();

				foreach ($users as $user) 
				{
					$statuspengisian = 'false';
					$timesheets = Timesheet::where('nip_pekerja', '=', $user->nip)->get();


					foreach ($timesheets as $timesheet)
					{
						if ($timesheet->tanggal == $today) 
						{
							$statuspengisian = 'true';
						}
					}

					if ($statuspengisian == 'false')
					{
						Mail::raw('Hai! Anda belum mengisi timesheet!', function($message)
						{
							$message->to('muhammad.ulil21@ui.ac.id')
							->from('no-reply@elmisapp.com')
							->subject('Pengisian Timesheet Hari Ini');
						});
					}
				}
			})->dailyAt($setnotif[0]['jam_notif']);
		}
		


		if(empty($settings)){

		}
		else{

			$schedule->call(function(){

				$excel = \App::make('excel');
				$t = Carbon::now();

				$x = $t->month;
				$cal = cal_info(0)['months'][$x];
				$document = \Excel::create('Rekap Timesheet '.$cal, function($excel) {


					$excel->sheet('Sheetname', function($sheet) {
						$users = User::all();
						$t = Carbon::now();
						$t2 = Carbon::now();

						$x = $t->month;

						$cal = cal_info(0)['months'][$x];

						$begin = $t->startOfMonth();
						$end = $t2->endOfMonth();


						$interval = new DateInterval('P1D');
						$daterange = new DatePeriod($begin, $interval ,$end);

						$tanggal = [];
						foreach( $daterange as $date) 
						{ 
							$dt = $date->format('Y-m-d');
							$dt2 = Carbon::createFromFormat('Y-m-d', $dt);
							if($dt2->isWeekday()){ $tanggal[] = $date->format('Y-m-d'); } 

						}

						$sheet->loadView('rekap.emailreport')->with('tanggal', $tanggal)->with('users', $users)->with('cal', $cal);
					});

				})->store('xlsx', false, true);



				Mail::raw('Rekap Timesheet terlampir. Silakan di download :)', function($message)
				{
					$t = Carbon::now();
					$x = $t->month;
					$cal = cal_info(0)['months'][$x];

					foreach($settings as $setting){
						$message->to($setting['email'])
						->from('no-reply@elmisapp.com', 'ELMIS')
						->subject('Report Timesheet Bulan '.$cal)
						->attach($_SERVER["DOCUMENT_ROOT"].'/b08/storage/exports/Rekap Timesheet '.$cal.'.xlsx');
					}
					
				});
			})->monthly();
		}*/
	}
}
