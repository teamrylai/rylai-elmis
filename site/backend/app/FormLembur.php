<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FormLembur extends Model {

	//
    protected $table = 'form_lembur';

    protected $primaryKey = 'kode';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['kode', 'jam_mulai','jam_selesai','keterangan'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    public $timestamps = false;

    public function form()
    {
        return $this->belongsTo('App\Form');
    }
}
