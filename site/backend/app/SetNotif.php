<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SetNotif extends Model {

	//

	protected $table = 'set_notifs';


    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['jam_notif'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;
}
