<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'project';

    protected $primaryKey = 'project_id';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = ['project_id','project_name']; 

	public $timestamps = false;

	public function user()
	{
		return $this->belongsToMany('App\User','project_user','project_id','nip');
	}



}