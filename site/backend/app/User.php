<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

    protected $primaryKey = 'nip';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = ['nip','username', 'email', 'password','nama','jabatan','no_telp','role_id','team_id','status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	protected $hidden = [];

	public $timestamps = false;

	public function role()
	{
		return $this->belongsTo('App\Role');
	}

	public function team()
	{
		return $this->belongsTo('App\Team');
	}

	public function project()
	{
		return $this->belongsToMany('App\Project', 'project_user', 'nip', 'project_id');
	}
		
	 public function timesheet()
    {
        return $this->hasMany('App\Timesheet','nip_pekerja','nip');
    }

    public function form()
    {
        return $this->hasMany('App\Form','nip_pekerja','nip');
    }    
}
