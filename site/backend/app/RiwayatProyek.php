<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RiwayatProyek extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'riwayat_proyek';

    protected $primaryKey = 'riwayat_proyek_id';
	
	protected $fillable = ['project_name'];
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	*/




}
