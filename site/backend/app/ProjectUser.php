<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectUser extends Model {

	//
	protected $table = 'project_user';


    protected $primaryKey = 'nip';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nip','project_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    public $timestamps = false;


}
