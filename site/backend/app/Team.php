<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model {

	//
    protected $table = 'team';


    protected $primaryKey = 'team_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['team_id', 'team_name', 'team_leader'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

}
