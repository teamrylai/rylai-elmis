<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Timesheet extends Model {

	//
	protected $table = 'timesheet';


    protected $primaryKey = 'timesheet_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['tanggal','waktu_mulai','waktu_selesai','kegiatan','status_absensi','status_isian','nip_pekerja', 'project_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
        

}
