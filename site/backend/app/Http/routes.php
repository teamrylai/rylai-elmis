<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//template url yang butuh login -> // Route::get('url',[ 'middleware'=>'auth','uses'=>NamaController@namaMethod]);

Route::get('/', 'HomeController@index');

Route::get('home', 'HomeController@index');
Route::post('home', 'HomeController@store');

Route::get('login', array('uses' => 'HomeController@showLogin'));

// route to process the form
Route::post('login', array('uses' => 'HomeController@doLogin'));



/*Route::resource('user', 'UserController');

Route::get('user',[ 'middleware'=>'auth','uses'=>'UserController@index']);

Route::get('user/create', 'UserController@create');

Route::get('user/create',[ 'middleware'=>'auth','uses'=> 'UserController@create']);
Route::post('user/',[ 'middleware'=>'auth','uses'=> 'UserController@store']);

Route::get('user/{id}', [ 'middleware'=>'auth','uses'=>'UserController@show']);
Route::get('user/{id}/edit',[ 'middleware'=>'auth','uses'=> 'UserController@edit']);
Route::patch('user/{id}',[ 'middleware'=>'auth','uses'=> 'UserController@update']);*/

// testt
Route::delete('user/{id}',[ 'middleware'=>'auth','uses'=> 'UserController@destroy']);

// Routes for Riwayat Pekerja
Route::get('riwayatpekerja', [ 'middleware'=>'auth','uses'=>'RiwayatPekerjaController@index']);
Route::get('riwayatpekerja/{id}', [ 'middleware'=>'auth','uses'=>'RiwayatPekerjaController@show']);

// Routes for Riwayat Proyek
Route::get('riwayatproyek', [ 'middleware'=>'auth','uses'=>'RiwayatProyekController@index']);
Route::get('riwayatproyek/{id}', [ 'middleware'=>'auth','uses'=>'RiwayatProyekController@show']);

Route::get('logout', array('uses' => 'HomeController@doLogout'));



Route::get('rekap', ['middleware' => 'auth', 'uses' => 'RekapController@index']);
Route::get('rekap/{id}', ['middleware' => 'auth', 'uses' => 'RekapController@show']);
Route::get('rekap/download/{id}', ['middleware' => 'auth', 'uses' => 'RekapController@download']);
Route::get('rekap/downloadtest/{id}', ['middleware' => 'auth', 'uses' => 'RekapController@testtableview']);


Route::get('formlembur', [ 'middleware' => 'auth', 'uses' => 'FormLemburController@index' ]);
Route::get('formlembur/create', [ 'middleware' => 'auth', 'uses' => 'FormLemburController@create' ]);
Route::post('formlembur/create', [ 'middleware' => 'auth', 'uses' => 'FormLemburController@store' ]);
Route::get('formlembur/formlist', [ 'middleware' => 'auth', 'uses' => 'FormLemburController@showFormList' ]);
Route::get('formlembur/formlist/{id}', [ 'middleware' => 'auth', 'uses' => 'FormLemburController@approve' ]);

Route::get('formlembur/download/{id}', [ 'middleware' => 'auth', 'uses' => 'FormLemburController@download' ]);

//Route::get('formlembur/formlist/{id}', [ 'middleware' => 'auth', 'uses' => 'FormLemburController@disapprove' ]);




Route::get('formcuti', ['middleware' => 'auth', 'uses' => 'FormCutiController@index']);
Route::get('formcuti/create', ['middleware' => 'auth', 'uses' => 'FormCutiController@create']);
Route::post('formcuti/create', ['middleware' => 'auth','uses'=> 'FormCutiController@store']);
Route::get('formcuti/download/{id}', [ 'middleware' => 'auth', 'uses' => 'FormCutiController@download' ]);


Route::get('isitimesheet/{date?}', 'TimesheetController@create');
Route::post('isitimesheet', 'TimesheetController@store');


//Routes for PROFILE menu
Route::get('profile', ['middleware'=>'auth', 'uses'=>'ProfileController@index']);
Route::get('profile/edit', ['middleware'=>'auth', 'uses'=>'ProfileController@edit']);
Route::patch('profile/edit', ['middleware'=>'auth', 'uses'=>'ProfileController@update']);
Route::get('uploadSignature', ['middleware'=>'auth', 'uses'=>function() 
	{
		return View::make('profile.uploadSignature');
	}]);
Route::post('profile/uploadSignature', ['middleware'=>'auth', 'uses'=>'ProfileController@uploadSignature']);
//PROFILE routes ends here

Route::get('testmethod', 'ProfileController@testmethod');
Route::get('denda', ['middleware'=>'auth', 'uses'=>'ProfileController@denda']);

//Routes for Pesan

Route::resource('pesan', 'PesanController');
Route::get('pesantest',[ 'middleware'=>'auth','uses'=>'PesanController@test']);
Route::get('pesan',[ 'middleware'=>'auth','uses'=>'PesanController@index']);
Route::get('pesan/create',[ 'middleware'=>'auth','uses'=> 'PesanController@create']);
Route::post('pesan/',[ 'middleware'=>'auth','uses'=> 'PesanController@store']);
Route::get('pesan/{id}', [ 'middleware'=>'auth','uses'=>'PesanController@show']);
Route::get('pesan/balas/{id}',[ 'middleware'=>'auth','uses'=> 'PesanController@balas']);
//Routes for Pesan


Route::get('statest',[ 'middleware'=>'auth','uses'=>'StatistikController@test']);
Route::get('statistik',[ 'middleware'=>'auth','uses'=>'StatistikController@index']);
Route::get('statistik/{id}', ['middleware' => 'auth', 'uses' => 'StatistikController@show']);

//route for email etc
Route::get('email',[ 'middleware'=>'auth','uses'=>'EmailController@index']);
Route::get('email/setting', [ 'middleware'=>'auth','uses'=>'EmailController@create']);
Route::post('emailrekap',[ 'middleware'=>'auth','uses'=>'EmailController@storerekap']);
Route::post('emailts',[ 'middleware'=>'auth','uses'=>'EmailController@storets']);



Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


/**
 * API SPECIFIC ROUTES
 */

Route::post('api/auth', function(){
	//csrf disabled, please check middleware/verifycsrftoken.php
		$rules = array(
	        'email'    => 'required|email',
	        'password' => 'required|alphaNum|min:5'
	    );

	    $validator = Validator::make(Input::all(), $rules);

	    if ($validator->fails()) {
            $errors = $validator->errors();
			$errormessage = '';
			foreach ($errors->getMessages() as $message) {
				$errormessage .=  $message[0] . ' ' ;
			}
			return App\Utils::responseFailed($errormessage);
        } else {
            $user = array(
                'email'   => Input::get('email'),
                'password'=> Input::get('password')
            );

            if (Auth::attempt($user)) {
            	$data = App\User::where('email', Input::get('email'))->take(1)->get();
                return App\Utils::responseSuccess($data,'login berhasil');
            } else {
                return App\Utils::responseFailed('User tidak ditemukan', 404);
            }
        }
	});

Route::get('api/timesheet/monthly', array('uses' => 'ApiController@monthlytimesheet'));
Route::get('api/timesheet/daily', array('uses' => 'ApiController@dailytimesheet'));
Route::get('api/project/user', array('uses' => 'ApiController@projectuser'));
Route::get('api/overtime/daily', array('uses' => 'ApiController@overtimelist'));
Route::get('api/overtime/monthly', array('uses' => 'ApiController@monthlyovertime'));
Route::get('api/leave/daily', array('uses' => 'ApiController@leavelist'));
Route::get('api/leave/monthly', array('uses' => 'ApiController@monthlyleave'));
Route::post('api/leave', array('uses' => 'ApiController@storeleave'));
Route::post('api/overtime', array('uses' => 'ApiController@storeovertime'));