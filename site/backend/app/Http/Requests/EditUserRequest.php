<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditUserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [

			//'nip'        => 'required|integer|min:10|unique:users',
			'nama'       => 'required',
            'email'      => 'required|email',
            'username'   => 'required',
            'jabatan'    => 'required',
            'no_telp'    => 'required|numeric',
            'role_id'    => 'required|integer',
            'team_id'    => 'required|integer'
		];
	}

}
