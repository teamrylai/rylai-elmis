<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateFormCuti extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'jenis_cuti' => 'required',
			'tanggal_mulai' => 'required|date',
			'tanggal_selesai' => 'sometimes|date|after:tanggal_mulai',
			'alasan' => 'required'

		];
	}

	public function validate_date($attribute, $value, $parameters){
	  $e_str = explode("-", $value);
	  if(count($e_str) === 3){

	    //expected format for date input is: Y-m-d
	    $year = $e_str[0];
	    $month = $e_str[1];
	    $day = $e_str[2];

	    return checkdate($month, $day, $year); //expected format for checkdate is: m-d-y
	  }
	  return false;
	}

}
