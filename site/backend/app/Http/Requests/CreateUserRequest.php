<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateUserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [

			'nip'        => 'required|integer|min:10|unique:users',
			'nama'       => 'required',
            'email'      => 'required|email|unique:users',
            'password'   => 'required|alphaNum|min:6|same:confirm_password',
            'confirm_password'   => 'required|alphaNum|min:6|same:password',
            'username'   => 'required',
            'jabatan'    => 'required',
            'no_telp'    => 'required|numeric',
            'role_id'    => 'required|integer',
            'team_id'    => 'required|integer'
		];
	}

}
