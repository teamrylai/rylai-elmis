<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditProfile extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'nama' => 'required',
            'email' => 'required|email',
            'confirm_password' => 'min:6|same:new_password',
            'new_password' => 'min:6|same:confirm_password',
            'no_telp' => 'required|numeric'
        ];
	}

}
