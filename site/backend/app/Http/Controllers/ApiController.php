<?php namespace App\Http\Controllers;
/**
 * ApiController.php
 * File handling all API calls
 * Not a good practice of a controller. Controller(s) should be as thin as possible
 * TODO : Move querie(s) to Model.
 */
use App\Utils;
use App\Timesheet;
use App\FormLembur;
use App\FormCuti;
use App\Project;
use App\User;
use App\Form;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use DateTime;
use DatePeriod;
use DateInterval;

use Validator;
use Input;

class ApiController extends Controller {

	public function index()
	{
		
	}

	public function monthlytimesheet(){
		$rules = array(
	        'nip'    => 'required',
	        'tanggal' => 'required',
	        'order' => 'required'
	    );

	    $validator = Validator::make(Input::all(), $rules);

	    if ($validator->fails()) {
            $errors = $validator->errors();
			$errormessage = '';
			foreach ($errors->getMessages() as $message) {
				$errormessage .=  $message[0] . ' ' ;
			}
			return Utils::responseFailed($errormessage);
        } else {
        	$nip = Input::get('nip');
        	$tanggal = Input::get('tanggal') . '%';
        	$sort = Input::get('order');
        	$data = Timesheet::where('nip_pekerja', '=', $nip)->where('tanggal', 'LIKE', $tanggal)->with('project')->orderBy('tanggal', $sort)->get();
        	return Utils::responseSuccess($data,'');
        }
	}

	public function dailytimesheet(){
		$rules = array(
	        'nip'    => 'required',
	        'tanggal' => 'required',
	        'order' => 'required'
	    );

	    $validator = Validator::make(Input::all(), $rules);

	    if ($validator->fails()) {
            $errors = $validator->errors();
			$errormessage = '';
			foreach ($errors->getMessages() as $message) {
				$errormessage .=  $message[0] . ' ' ;
			}
			return Utils::responseFailed($errormessage);
        } else {
        	$nip = Input::get('nip');
        	$tanggal = Input::get('tanggal');
        	$sort = Input::get('order');
        	$data = Timesheet::where('nip_pekerja', '=', $nip)->where('tanggal', 'LIKE', $tanggal)->with('project')->orderBy('tanggal', $sort)->get();
        	return Utils::responseSuccess($data,'');
        }
	}

	public function projectuser(){
		$rules = array(
	        'nip'    => 'required'
	    );

	    $validator = Validator::make(Input::all(), $rules);

	    if ($validator->fails()) {
            $errors = $validator->errors();
			$errormessage = '';
			foreach ($errors->getMessages() as $message) {
				$errormessage .=  $message[0] . ' ' ;
			}
			return Utils::responseFailed($errormessage);
        } else {
        	$nip = Input::get('nip');
        	$data = User::where('nip', $nip)->with('project')->get();
        	return Utils::responseSuccess($data,'');
        }
	}

	public function overtimelist(){
		$rules = array(
	        'nip'    => 'required',
	        'tanggal' => 'required',
	        'order' => 'required'
	    );

	    $validator = Validator::make(Input::all(), $rules);

	    if ($validator->fails()) {
            $errors = $validator->errors();
			$errormessage = '';
			foreach ($errors->getMessages() as $message) {
				$errormessage .=  $message[0] . ' ' ;
			}
			return Utils::responseFailed($errormessage);
        } else {
        	$nip = Input::get('nip');
        	$tanggal = Input::get('tanggal');
        	$sort = Input::get('order');

        	$forms = Form::where('nip_pekerja', '=', $nip)->where('jenis_form', '=', 'lembur')->where('created_at', '>=', $tanggal . ' 00:00:00')->where('created_at', '<=', $tanggal . ' 23:59:59')->orderBy('created_at', $sort)->get();
        	foreach ($forms as $form) {
        		$form_data = FormLembur::where('kode', '=', $form->kode)->get();
        		$form['detail_lembur'] = $form_data;
        	}
        	return Utils::responseSuccess($forms,'');
        }
	}

	public function monthlyovertime(){
		$rules = array(
	        'nip'    => 'required',
	        'tanggal' => 'required',
	        'order' => 'required'
	    );

	    $validator = Validator::make(Input::all(), $rules);

	    if ($validator->fails()) {
            $errors = $validator->errors();
			$errormessage = '';
			foreach ($errors->getMessages() as $message) {
				$errormessage .=  $message[0] . ' ' ;
			}
			return Utils::responseFailed($errormessage);
        } else {
        	$nip = Input::get('nip');
        	$tanggal = Input::get('tanggal');
        	$sort = Input::get('order');
        	$forms = Form::where('nip_pekerja', '=', $nip)->where('jenis_form', '=', 'lembur')->where('created_at', 'LIKE', $tanggal . '%')->orderBy('created_at', $sort)->get();
        	foreach ($forms as $form) {
        		$form_data = FormLembur::where('kode', '=', $form->kode)->get();
        		$form['detail_lembur'] = $form_data;
        	}
        	return Utils::responseSuccess($forms,'');
        }
	}

	public function storeovertime(){
		$input = Input::all();

		$rules = array(
	        'nip'    => 'required',
	        'tanggal' => 'required',
	        'waktumulai' => 'required',
	        'waktuselesai' => 'required',
	        'keterangan' => 'required',
	    );

	    $validator = Validator::make($input, $rules);

	    if ($validator->fails()) {
            $errors = $validator->errors();
			$errormessage = '';
			foreach ($errors->getMessages() as $message) {
				$errormessage .=  $message[0] . ' ' ;
			}
			return Utils::responseFailed($errormessage);
        } else {
        	$tanggal = new DateTime( $input['tanggal'] );

        	Form::create([
				'jenis_form' => 'lembur',
				'status' => 'pending',
				'nip_pekerja' => $input['nip'],
				'created_at' => $tanggal->format('Y-m-d H:i:s')
			]);

			$arrayform = Form::orderBy('created_at', 'DESC')->take(1)->get();

			$form = $arrayform[0];

			$formlembur = FormLembur::create([
				'kode' => $form['kode'],
				'jam_mulai' => $input['waktumulai'],
				'jam_selesai' => $input['waktuselesai'],
				'keterangan' => $input['keterangan']
			]);
        	return Utils::responseSuccess($formlembur,'');
        }
	}

	public function leavelist(){
		$rules = array(
	        'nip'    => 'required',
	        'tanggal' => 'required',
	        'order' => 'required'
	    );

	    $validator = Validator::make(Input::all(), $rules);

	    if ($validator->fails()) {
            $errors = $validator->errors();
			$errormessage = '';
			foreach ($errors->getMessages() as $message) {
				$errormessage .=  $message[0] . ' ' ;
			}
			return Utils::responseFailed($errormessage);
        } else {
        	$nip = Input::get('nip');
        	$tanggal = Input::get('tanggal');
        	$sort = Input::get('order');
        	$forms = Form::where('nip_pekerja', '=', $nip)->where('jenis_form', '=', 'cuti')->where('created_at', '>=', $tanggal . ' 00:00:00')->where('created_at', '<=', $tanggal . ' 23:59:59')->orderBy('created_at', $sort)->get();
        	foreach ($forms as $form) {
        		$form_data = FormCuti::where('kode', '=', $form->kode)->get();
        		$form['detail_cuti'] = $form_data;
        	}
        	return Utils::responseSuccess($forms,'');
        }
	}

	public function monthlyleave(){
		$rules = array(
	        'nip'    => 'required',
	        'tanggal' => 'required',
	        'order' => 'required'
	    );

	    $validator = Validator::make(Input::all(), $rules);

	    if ($validator->fails()) {
            $errors = $validator->errors();
			$errormessage = '';
			foreach ($errors->getMessages() as $message) {
				$errormessage .=  $message[0] . ' ' ;
			}
			return Utils::responseFailed($errormessage);
        } else {
        	$nip = Input::get('nip');
        	$tanggal = Input::get('tanggal');
        	$sort = Input::get('order');

        	$forms = Form::where('nip_pekerja', '=', $nip)->where('jenis_form', '=', 'cuti')
        			->leftJoin('form_cuti', 'form_cuti.kode', '=', 'form.kode')
        			->where('form_cuti.tgl_mulai', 'LIKE', $tanggal . '%')
        			->orderBy('form_cuti.tgl_mulai', $sort)
        			->get();
        	
        	return Utils::responseSuccess($forms,'');
        }
	}

	public function storeleave(){
		$input = Input::all();

		$rules = array(
	        'nip'    => 'required',
	        'jenis_cuti' => 'required',
	        'tanggal_mulai' => 'required',
	        'tanggal_selesai' => 'required',
	        'alasan' => 'required',
	    );

	    $validator = Validator::make($input, $rules);
	    
	    if ($validator->fails()) {
            $errors = $validator->errors();
			$errormessage = '';
			foreach ($errors->getMessages() as $message) {
				$errormessage .=  $message[0] . ' ' ;
			}
			return Utils::responseFailed($errormessage);
        } else {
        	$jc = $input['jenis_cuti'];
        	//kenapa tambah 1 ?
			$jeniscuti = $input['jenis_cuti'] + 1;

			$tanggal_mulai = (new DateTime($input['tanggal_mulai']));

        	Form::create([
			'jenis_form' => 'cuti',
			'status' => 'pending',
			'nip_pekerja' => $input['nip']
			]);

			$arrayform = Form::orderBy('created_at', 'DESC')->take(1)->get();

			$form = $arrayform[0];

			if($input['tanggal_selesai'] == NULL){
				$input['tanggal_selesai'] = $input['tanggal_mulai'];
			}

			$tgl_mulai = date('Y-m-d', strtotime($input['tanggal_mulai']));
			$tgl_selesai = date('Y-m-d', strtotime($input['tanggal_selesai']));

			$formcuti = FormCuti::create([
				'kode' => $form['kode'],
				'jenis_cuti'=> $jeniscuti,
				'tgl_mulai' => $tgl_mulai,
				'tgl_selesai' => $tgl_selesai,
				'alasan' => $input['alasan'],
				]);
			$begin = new DateTime( $tgl_mulai );
			$end = new DateTime( $tgl_selesai );
			$end = $end->modify( '+1 day' ); 

			$interval = new DateInterval('P1D');
			$daterange = new DatePeriod($begin, $interval ,$end);

			foreach( $daterange as $date) { $dates[] = $date->format('Y-m-d'); }

			foreach( $dates as $date )
			{
				Timesheet::create([
				'tanggal' => $date,
				'status_absensi' => 'C',
				'waktu_mulai' => '00:00:00',
				'waktu_selesai' => '23:59:59',
				'kegiatan' => 'Cuti',
				'status_isian' => 'C', 
				'nip_pekerja' => $input['nip'],
				'project_id' => '1'
				]);
			}
        	return Utils::responseSuccess($formcuti,'');
        }
	}

}
