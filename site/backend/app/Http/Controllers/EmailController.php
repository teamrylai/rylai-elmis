<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\User;
use App\SetNotif;
use App\Setting;


class EmailController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$settings = Setting::all();
		$setnotif = SetNotif::all();

		//return $_SERVER["DOCUMENT_ROOT"];

        //return redirect('email.create');
        return view('email.index', compact('settings','setnotif'));	
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{	
		//
		$users = User::all();
		$notif = SetNotif::all();
		$settingemail = Setting::all();

		if ($notif->isEmpty() || $settingemail->isEmpty()) {

			return view('email.initiate',compact('users'));
		}

		//return $settingemail;


		 if(is_array($settingemail)){

		 	return $settingemail;

		 }
		//return $settingemail;

		$datanotif =  $notif[0];

		//return $datanotif;
		//return $notif;
	
		//return view('email.create', compact('users','notif','setting'));
		return view('email.create', compact('users','datanotif','settingemail'));
		//return view('email.create', compact('datanotif'));


	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function storets(Requests\SettingTSRequest $request)
	{	

		
		$input = $request->all();
        $jam = $input['jam_notif'];
        $setnotif = SetNotif::all();

        

        
        if (! $setnotif->isEmpty()) {

        	\DB::table('set_notifs')->delete();

        	$input = SetNotif::create([
			'jam_notif'=> $jam,
			]);
        }
        else{

        	$input = SetNotif::create([
			'jam_notif'=> $jam,
			]);

        }

        

     //    foreach ($accounttypes as $accounttype) {
     //    DB::insert('INSERT INTO tb_accounts (accounttype,client) VALUES (?,?)', array($accounttype,Input::get('client'));
    	// }

	    //done save setting redirect ke view dgn updated data  dari db
	    $settings = Setting::all();
		$setnotif = SetNotif::all();



        \Session::flash('flash_setting','Pengaturan berhasil disimpan.');
        //return redirect('email.create');
        return \Redirect::to('email')->with('settings', $settings)->with('setnotif', $setnotif);
        //return view('email.index', compact('settings','setnotif'));
	}

	public function storerekap(Requests\SettingEmailRequest $request)
	{
			
		$input = $request->all();
		//return $input;
        $arrayemail =$input['emails'];
        $settings = Setting::all();
        //return $arrayemail;
        if(is_array($arrayemail)) 
    	{

    		//return "ggwp array email ";
    		//return $arrayemail;
			//return $arrayemail[0];
			\DB::table('settings')->delete();

			for($i=0;$i < count($arrayemail);$i++)
	        {
	        	$input = Setting::create([
				'email'=> $arrayemail[$i],
				]);


	        }
	    }

     //    foreach ($accounttypes as $accounttype) {
     //    DB::insert('INSERT INTO tb_accounts (accounttype,client) VALUES (?,?)', array($accounttype,Input::get('client'));
    	// }

	    //done save setting redirect ke view dgn updated data  dari db
	    $settings = Setting::all();
		$setnotif = SetNotif::all();



        \Session::flash('flash_setting','Pengaturan berhasil disimpan.');
        //return redirect('email.create');
        return \Redirect::to('email')->with('settings', $settings)->with('setnotif', $setnotif);

        //return view('email.index', compact('settings','setnotif'));



	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
