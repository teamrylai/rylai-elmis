<?php namespace App\Http\Controllers;
use App\Pesan;
use App\Adminreply;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;




use Illuminate\Support\Facades\Auth;

class PesanController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
    }



	public function test(){
		$currentUserRoleId = Auth::user()->role->id;
		if($currentUserRoleId =='1'){

			return "admin";
		}
		else{

			return 'bukan admin';		
		}

	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$users = User::all();
		$currentUserRoleId = Auth::user()->role->id;
		if($currentUserRoleId =='1'){
			//semua admin mendapat semua pesan dari user
			$messages = Pesan::paginate(10);
			return view('pesan.index', compact('messages','users'));	

			
		}

		else
		{
			//dieliminasi sesuai nip usernya
			
			$nip = Auth::user()->nip;
			$messages = Adminreply::where('id_penerima','=',$nip)->paginate(10);

			//$messages = Adminreply::paginate(10);

			//return $messages->toArray();
			return view('pesan.index2', compact('messages'));	

		}

		

		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//

		$user = \Auth::user();



		$currentUserRoleId = Auth::user()->role->id;
		$usersdata = [];
		if($currentUserRoleId =='1'){

			//admin tidak dihitung yg mendapat pesan
			$users = User::where('role_id','!=',1)->get();
			foreach ($users as $us) {
				$usersdata[$us['nip']] = $us['nama'];
			}

			//return $usersdata;
			return view('pesan.create',compact('users'));

		}
		else{

			return View::make('pesan.kirim');		
		}
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//
		$currentUserRoleId = Auth::user()->role->id; 
		if($currentUserRoleId =='1'){
			$input = $request->all();
		//return $input;
		        $arraynip =$input['arrpenerima'];
		        //return $arrayemail;
		        if(is_array($arraynip)) 
		    	{

		    		//return "ggwp array email ";
		    		//return $arrayemail;
					//return $arrayemail[0];

					for($i=0;$i < count($arraynip);$i++)
			        {
			        	$input = Adminreply::create([
						'id_penerima'=> $arraynip[$i],
						'subjek'=>$input['subjek'],
						'isi'=>$input['isi'],
						]);
			        }
			    }
	    ////////////////////////////////

			// $input = Adminreply::create([
			// 'id_penerima'=> $input['penerima'],
			// 'subjek'=>$input['subjek'],
			// 'isi'=>$input['isi'],
			// 	]);


      		// Adminreply::create($input);
      		
      		}

      	else {
			$input = $request->all();
      		// Pesan::create($input);
      		
			$nip = Auth::user()->nip;
			$input = Pesan::create([
			'id_pengirim'=> $nip,
			'subjek'=>$input['subjek'],
			'isi'=>$input['isi'],
				]);
      		// Adminreply::create($input);
      		
      		}
      		\Session::flash('flash_pesan','Pesan berhasil dikirim.');
      		return redirect('pesan');
		
        //return redirect('pesan');
        //return $input;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$currentUserRoleId = Auth::user()->role->id;
		if($currentUserRoleId =='1'){

				$message = Pesan::findOrFail($id);

				$user = User::findOrFail($message->id_pengirim);

				if(is_null($message) || is_null($user)){
					abort(404);
				}

				return view('pesan.show', compact('message', 'user'));
		}

		else{

			$message = Adminreply::findOrFail($id);

				if(is_null($message) ){
					abort(404);
				}

				return view('pesan.show2', compact('message'));		
		}


		
	}

	public function balas($id)
	{
		//
		$currentUserRoleId = Auth::user()->role->id;
		if($currentUserRoleId =='1'){

				$message = Pesan::findOrFail($id);

				if(is_null($message)){
					abort(404);
				}

				return view('pesan.reply', compact('message'));
		}

		else{

			return View::make('pesan.kirim');		
		}


		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
