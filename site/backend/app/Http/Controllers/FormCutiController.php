<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\FormCuti;
use App\Form;
use App\Project;
use App\Timesheet;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use DateTime;
use DatePeriod;
use DateInterval;
use App\User;


class FormCutiController extends Controller {


	public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		/*$tanggal = Carbon::now();
		$jeniscuti = \Auth::form()->form_cuti->jenis_cuti;
		$cutimulai = \Auth::form()->form_cuti->tgl_mulai;
		$cutiselesai = \Auth::form()->form_cuti->tgl_selesai;*/

		$user = \Auth::user();
		$forms = Form::where('nip_pekerja', '=', $user->nip)->where('jenis_form', '=', 'cuti')->get();
		/*$formcuti = [];
		
				foreach ( $forms as $form ) {
					if ( $nip == $form->nip_pekerja) {
						array_push($formcuti, $form);
					}
				}*/
		//return $forms;
		return view('formcuti.listdownload')->with('forms', $forms);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('formcuti.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\CreateFormCuti $request)
	{
		
		$input = $request->all();

		$jc = $input['jenis_cuti'];

		$jeniscuti = $input['jenis_cuti'] + 1;

		$user = \Auth::user();

		Form::create([
			'jenis_form' => 'cuti',
			'status' => 'pending',
			'nip_pekerja' => $user['nip'] 
			]);

		$arrayform = Form::orderBy('created_at', 'DESC')->take(1)->get();

		$form = $arrayform[0];

		if($input['tanggal_selesai'] == NULL){
			$input['tanggal_selesai'] = $input['tanggal_mulai'];
		}

		$tgl_mulai = date('Y-m-d', strtotime($input['tanggal_mulai']));
		$tgl_selesai = date('Y-m-d', strtotime($input['tanggal_selesai']));

		$formcuti = FormCuti::create([
			'kode' => $form['kode'],
			'jenis_cuti'=> $jeniscuti,
			'tgl_mulai' => $tgl_mulai,
			'tgl_selesai' => $tgl_selesai,
			'alasan' => $input['alasan'],
			]);
		$begin = new DateTime( $tgl_mulai );
		$end = new DateTime( $tgl_selesai );
		$end = $end->modify( '+1 day' ); 

		$interval = new DateInterval('P1D');
		$daterange = new DatePeriod($begin, $interval ,$end);

		foreach( $daterange as $date) { $dates[] = $date->format('Y-m-d'); }

		foreach( $dates as $date )
		{
			Timesheet::create([
			'tanggal' => $date,
			'status_absensi' => 'C',
			'waktu_mulai' => '00:00:00',
			'waktu_selesai' => '23:59:59',
			'kegiatan' => 'Cuti',
			'status_isian' => 'C', 
			'nip_pekerja' => $user['nip'],
			'project_id' => Project::first()->project_id
			]);
		}
		flash()->overlay('Form cuti berhasil disimpan', 'Berhasil');

		return redirect('formcuti');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function showListDownload()
	{
		
		//return $formlembur;
	}//

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function download($id)
	{
		$user = \Auth::user();

		$allform = Form::all();

		$forms = [];

		$tl = User::where('team_id', '=', $user->team_id)->where('role_id', '=', '2')->get();

		if(count($tl) > 0){
			$teamleader = $tl[0];
		}
		else{
			$teamleader = [];
		}
		
		//return $teamleader;

		foreach ($allform as $key => $value) {
			if($value['nip_pekerja'] == $user['nip']){
				$forms[$value['kode']] = $value;
			}
		}

		$form = Form::findOrFail($id);	

		$pdf = \App::make('dompdf');

		$pdf = $pdf->loadView('formcuti.download', compact('form', 'user', 'teamleader'));

		$haha = [];

		return $pdf->stream();
	}

	public function destroy($id)
	{
		//
	}

}
