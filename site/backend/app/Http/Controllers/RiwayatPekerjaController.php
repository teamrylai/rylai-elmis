<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class RiwayatPekerjaController extends Controller {

	public function __construct()
    {
        $this->middleware('admin', ['only' => 'index']);
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::paginate(10);

		$users->setPath('riwayatpekerja');

		return view('riwayatpekerja.index', compact('users'));
	}

	public function show($id)
	{
		$user = User::findOrFail($id);

		$self = \Auth::user();

		if($self->nip != $id && $self->role_id != '1')
		{
			flash()->overlay('Anda tidak berhak mengakses halaman tersebut', 'Dilarang');
			return redirect('home');
		}

		if(is_null($user)){
			abort(404);
		}

		return view('riwayatpekerja.show', compact('user')); 
	}
		

/*	public function proyek()
	{
		$projects = 
	}*/
		

}
