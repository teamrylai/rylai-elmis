<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use View;
use DateTime;
use DatePeriod;
use DateInterval;

use Carbon\Carbon;
use Carbon\CarbonInterval;

use App\Timesheet;
use App\Project;
use App\User;

class HomeController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {        
        $this->denda();
        $tanggal = Carbon::now();        
        setlocale(LC_TIME, 'Indonesian');



        $tsheethariini = Timesheet::where('nip_pekerja', \Auth::user()->nip)
        ->where('tanggal', Carbon::now()->toDateString())
        ->get(); 

        $begin = Carbon::now()->startOfMonth();        
        $end = Carbon::today();
        
        $interval = new DateInterval('P1D');
        $daterange = new DatePeriod($begin, $interval ,$end);

        $missedtimesheet = [];

        foreach($daterange as $date)
        {
            $x = Timesheet::where('nip_pekerja', \Auth::user()->nip)
                            ->where('tanggal', $date)->get();


            if($x->isEmpty() && Carbon::instance($date)->isWeekday())
            {
                $missedtimesheet[] = Carbon::instance($date);
            }
        }
        
        $denda = \Auth::user()->denda;

        if(\Auth::user()->role_id < 3)
        {
            $projectuser = Project::all();
        }
        else
        {
            $projectuser = \Auth::user()->project;
        }
        

        return view('home')
                ->with('tanggal', $tanggal)
                ->with('tsheethariini', $tsheethariini)
                ->with('denda', $denda)
                ->with('missedtimesheet', $missedtimesheet)
                ->with('projectuser', $projectuser);
    }

    public function showLogin()
    {
        // show the form
        return View::make('login');
    }

    public function doLogin()
    {
        // process the form dan validate

        $rules = array(
            'email'    => 'required|email',
            'password' => 'required|alphaNum|min:5'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('login')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            $user = array(
                'email'   => Input::get('email'),
                'password'=> Input::get('password')
            );            

            if (Auth::attempt($user)) {
                return Redirect::to('/', $user);
            } else {
                return Redirect::to('login');
            }
        }

    }


    public function doLogout()
    {
        //doLogout clear session
        //

        return Redirect::to('login');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $user = \Auth::user();

        $status_isian;      
        if($input['status_absensi'] == 'S')
        {
            $status_isian = 'S';
        }
        else{
            $status_isian = 'OK';
        }

        $timesheet = Timesheet::create([
            'tanggal' => date('Y-m-d', strtotime($input['tanggal'])),           
            'waktu_mulai' => $input['waktumulai'],
            'waktu_selesai' => $input['waktuselesai'],
            'kegiatan' => $input['kegiatan'],
            'status_isian' => $status_isian, 
            'status_absensi' => $input['status_absensi'],
            'nip_pekerja' => $user['nip'],
            'project_id' => $input['project']
            ]);


        flash()->overlay('Timesheet berhasil disimpan!', 'Berhasil');
        $this->denda();
        return redirect('home');
    }

    //meng-update denda terbaru
    public function denda(){

        $currentdenda = \Auth::user()->denda;
        $nip = \Auth::user()->nip;
        $user = User::find($nip);
        $counter = 0;

        //asumsi denda perhari 20000
        $fine = 20000;
        
        $timesheet = Timesheet::where('nip_pekerja', '=', $user['nip'])->whereBetween('tanggal',[Carbon::now()->startofMonth(),Carbon::now()])->get();
        
        $tsvalid = [];
        foreach ($timesheet as $ts) {
            $dt = $ts['tanggal'];
            $dt2 = Carbon::createFromFormat('Y-m-d', $dt);
            if($dt2->isWeekday()){ $tsvalid[] = $ts; }
        }


        $jumlahngisi = count($tsvalid)*$fine;
        
        $interval = CarbonInterval::days(1);
        $begin  = Carbon::now()->startofMonth();
        $end = Carbon::now();

        $daterange = new DatePeriod($begin, $interval ,$end);

        $tanggal = [];
        foreach( $daterange as $date) 
        { 
            $dt = $date->format('Y-m-d');
            $dt2 = Carbon::createFromFormat('Y-m-d', $dt);
            if($dt2->isWeekday()){ $tanggal[] = $date; } 

        }

        $jumlahseharusnya = count($tanggal)*$fine;

        $denda = $jumlahseharusnya - $jumlahngisi;

        
        $user->denda = $denda;
        $user->save();        
    }

}
