<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Team;
use App\Role;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;



class UserController extends Controller {


	public function __construct()
    {
        $this->middleware('admin');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::paginate(10);
		$users->setPath('user');

		return view('user.index', compact('users'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//return view('user.create');
		$rs = Role::all();
		$ts = Team::all();


		
		$teams = [];
		$roles = [];

		foreach($rs as $role){
			$roles[$role['id']] = $role['role_name'];
		}
		foreach($ts as $team){
			$teams[$team['team_id']] = $team['team_name'];
		}

        return view('user.create', compact('roles', 'teams'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
//	public function store()
//	{
//		$input = Request::all();
//		User::create($input);
//
//		return redirect('user');

        public function store(Requests\CreateUserRequest $request)
    {
        $input = $request->all();
        $password = $input['password'];
        $pass = \Hash::make($password);
        $input['password'] = $pass;

        User::create($input);
        \Session::flash('flash_message1','User berhasil dibuat.');
        return redirect('user');

    }




	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::findOrFail($id);

		if(is_null($user)){
			abort(404);
		}

		return view('user.show', compact('user'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
        $user = User::findOrFail($id);

        return view('user.edit',compact('user'));

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Requests\EditUserRequest $request)
	{
		    $user=User::findOrFail($id);
            $user->update($request->all());


            \Session::flash('flash_message2','Data user berhasil diubah.');
            return redirect('user');

            //sesuai fillable di model user, User::create($input);

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	// testtt
	public function destroy($id)
	{
		//
        $user=User::findOrFail($id);
        $user->delete();

        \Session::flash('deletemessage', 'Berhasil menghapus user!');
        return \Redirect::to('user');
	}

}
