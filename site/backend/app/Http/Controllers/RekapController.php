<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Timesheet;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use DateTime;
use DatePeriod;
use DateInterval;
use App\User;

use Illuminate\Http\Request;

class RekapController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin', ['only' => 'download']);
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{		
		return \Redirect::to('rekap/'.Carbon::now()->month);				
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = \Auth::user();
		$users = [];

		if($user['role_id'] == 2)
		{
			$users = User::where('team_id', '=', $user['team_id'])->get();
		}
		if($user['role_id'] == 1)
		{
			$users = User::all();
		}
	

		$t = Carbon::now();
		$t->month = $id;
		$t2 = Carbon::now();
		$t2->month = $id;
		$t3 = Carbon::now();
		$t3->month = $id;
		$x = $t3->month;
		$timesheet = Timesheet::all();
		
		$cal = cal_info(0)['months'];

		$begin = $t->startOfMonth();
		if($t2->month == Carbon::now()->month){
			$end = $t2->startOfDay();
			$end = $end->modify('+1 day');
		}
		else{
			$end = $t2->endOfMonth();
		}
		
		$interval = new DateInterval('P1D');
		$daterange = new DatePeriod($begin, $interval ,$end);

		$dates = [];
		foreach( $daterange as $date) { $dates[] = $date->format('Y-m-d'); }
		
		$tanggal = [];
		foreach( $daterange as $date) 
		{ 
			$dt = $date->format('Y-m-d');
			$dt2 = Carbon::createFromFormat('Y-m-d', $dt);
			if($dt2->isWeekday()){ $tanggal[] = $date->format('Y-m-d'); }
		}

		$beginyear = $t3->startOfYear();

		$intervalmonth = new DateInterval('P1M');
		$end2 = Carbon::now();

		$monthrange = new DatePeriod($beginyear, $intervalmonth, $end2);

		$months = [];
		foreach( $monthrange as $date) { $months[] = $date; }

		$bulans = [];
		foreach( $months as $month ) { 
			$bulans[$month->format('n')] = $cal[$month->format('n')]; 
		}		


		return view('rekap.test')->with('user', $user)->with('test', $id)->with('bulans', $bulans)->with('x', $x)->with('tanggal', $tanggal)->with('timesheet', $timesheet)->with('users', $users);
			
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function download($id)
	{
		//return view('rekap.download')->with('timesheet', $timesheet)->with('dates', $dates)->with('tanggal', $tanggal)->with('users', $users);
		$excel = \App::make('excel');
		$GLOBALS['id'] = $id;
		$cal = cal_info(0)['months'][$GLOBALS['id']];
		$document = \Excel::create('Rekap Timesheet '.$cal, function($excel) {

			$excel->sheet('Keterangan', function($sheet) {
				$sheet->setOrientation('landscape');
				$sheet->setPageMargin(0.25);
		    	$sheet->loadView('rekap.downloadketerangan');
		    });
		    
		    $excel->sheet('Rekap', function($sheet) {
		    	$users = User::all();
				$t = Carbon::now();
				$t->month = $GLOBALS['id'];
				$t2 = Carbon::now();
				$t2->month = $GLOBALS['id'];
				$t3 = Carbon::now();
				$t3->month = $GLOBALS['id'];
				$x = $t3->month;

				$cal = cal_info(0)['months'][$x];

				$begin = $t->startOfMonth();
				if($t2->month == Carbon::now()->month){
					$end = $t2->startOfDay();
					$end = $end->modify('+1 day');
				}
				else{
					$end = $t2->endOfMonth();
				}
				

				$interval = new DateInterval('P1D');
				$daterange = new DatePeriod($begin, $interval ,$end);

				$tanggal = [];
				foreach( $daterange as $date) 
				{ 
					$dt = $date->format('Y-m-d');
					$dt2 = Carbon::createFromFormat('Y-m-d', $dt);
					if($dt2->isWeekday()){ $tanggal[] = $date->format('Y-m-d'); } 

				}

				$sheet->setOrientation('landscape');
				$sheet->setPageMargin(0.25);

		        $sheet->loadView('rekap.download')->with('tanggal', $tanggal)->with('users', $users)->with('cal', $cal);
		    });

		    

		})->export('xlsx');

		//return $document['full'];
	}

	public function testtableview($id)
	{
        $user = User::findOrFail($id);
        $form = $user->form;
        $formcuti = $form[1]->formcuti;

        return view('rekap.test', compact('user', 'formcuti', 'form'));

        $pdf = \App::make('dompdf');
        $pdf->loadView('rekap.test', compact('user', 'formcuti', 'form'));
        return $pdf->stream();
	}
		

}
