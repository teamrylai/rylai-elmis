<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Khill\Lavacharts\Lavacharts;

use App\Timesheet;
use App\Project;
use App\User;



class StatistikController extends Controller {



	public function test(){


		return view('statistik.test');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	

		return \Redirect::to('statistik/all');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$timesheet = Timesheet::all();
		//$projects = Project::all();

		if($id == 'all')
		{
			$projects = Project::all();
		}
		else{
			$projects = Project::where('project_id', $id)->get();
			//return $projects;
		}
		

		$users = User::all();

		$listproject = [];


		foreach ($projects as $project) {			
			$lava = new Lavacharts;
			$statproject  = $lava->DataTable();

			$lava->HorizontalAxis([
				'minValue' => 0,
				'maxValue' => 500,
				]);

			$statproject->addStringColumn($project['project_name'])
			->addNumberColumn('Hour Spent');			
			foreach ($project->user as $user) 
			{				
				$durasi = 0;
				$arrtimesheet = [];
				$arrtimesheet = $user->timesheet;


				foreach ($arrtimesheet as $tsdata) {
					if ($tsdata->project_id == $project->project_id) {
						$mulai = $tsdata->waktu_mulai;
						$selesai = $tsdata->waktu_selesai;
						$durasi += $selesai - $mulai;
					}
				}
				$statproject->addRow(array($user['nama'], $durasi));
			}

			$barchart = $lava->BarChart('Hour Spent')
			->setOptions(array(
				'datatable' => $statproject,
				'title' => $project['project_name']
				));

			$listproject[$project['project_name']] = $lava;
		}

		$pj = Project::all();
		$arrproject = [];
		$arrproject['all'] = 'Semua Proyek';
		foreach($pj as $project){
			$arrproject[$project->project_id] = $project->project_name;
		}

		$default = $id;

	


		//return $arrproject;
		return view('statistik.index')->with('listproject', $listproject)->with('arrproject',$arrproject)->with('default', $default);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
