<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Timesheet;

use App\Project;

class TimesheetController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new timesheet.
	 *
	 * @return Response
	 */
	public function create($date = null)
	{
		$user = \Auth::user();		
		//dd($user->project);
		//Admin and team leader role get assigned all project
		if($user->role_id == 1 || $user->role_id == 2)
		{
			$projectuser = Project::all();
		}
		else
		{
			$projectuser = $user->project;
		}


		return view('timesheet.create', compact('projectuser', 'date'));
	}	

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\CreateTimesheet $request)
	{
		$input = $request->all();
		$user = \Auth::user();

		setlocale(LC_TIME, 'Indonesian'); //set time to indonesian

		$status_isian;		
		if($input['status_absensi'] == 'S')
		{
			$status_isian = 'S';
		}
		else{
			$status_isian = 'OK';
		}

		//search database jika sudah ada timesheet pada tanggal yang dipilih
		$timesheets = Timesheet::where('nip_pekerja', \Auth::user()->nip)
        ->where('tanggal', Carbon::parse($input['tanggal'])->toDateString())
        ->orderBy('waktu_selesai', 'desc')
        ->take(1)
        ->get();

        //Jika akhir pekan
        if(Carbon::parse($input['tanggal'])->isWeekend())
        {
        	\Session::flash('flash_pesan','Hari Sabtu dan Minggu libur.');
			return redirect('isitimesheet');
        }

        //Jika pada hari yang dipilih belum mengisi
    	else if($timesheets->isEmpty())
        {
            $waktumulai = Carbon::createFromTime(10, 0, 0, null);
            $waktuselesai = $waktumulai->copy();
            $waktuselesai->addHours($input['jam'])->addMinutes($input['menit']);

            $timesheet = Timesheet::create([
			'tanggal' => date('Y-m-d', strtotime($input['tanggal'])),			
			'waktu_mulai' => $waktumulai,
			'waktu_selesai' => $waktuselesai,
			'kegiatan' => $input['kegiatan'],
			'status_isian' => $status_isian, 
			'status_absensi' => $input['status_absensi'],
			'nip_pekerja' => $user['nip'],
			'project_id' => $input['project']
			]);

		
			\Session::flash('flash_pesan','Timesheet sudah dibuat.');
			return redirect('isitimesheet');
        }
        else
        {
        	foreach ($timesheets as $ts)
        	{
        		//Jika sedang ambil cuti -> batalkan
        		if($ts['status_absensi'] == 'C' || $ts['status_isian'] == 'C')
		        {
		        	\Session::flash('flash_pesan','Anda sedang cuti pada hari itu.');
					return redirect('isitimesheet');
		        }
		        //Jika pada hari itu sudah mengisi
        		else
    			{
    				$waktumulai = Carbon::parse($ts['waktu_selesai']);
	        		$waktuselesai = $waktumulai->copy();
	            	$waktuselesai->addHours($input['jam'])->addMinutes($input['menit']);

	            	$timesheet = Timesheet::create([
					'tanggal' => date('Y-m-d', strtotime($input['tanggal'])),			
					'waktu_mulai' => $waktumulai,
					'waktu_selesai' => $waktuselesai,
					'kegiatan' => $input['kegiatan'],
					'status_isian' => $status_isian, 
					'status_absensi' => $input['status_absensi'],
					'nip_pekerja' => $user['nip'],
					'project_id' => $input['project']
					]);
				
					\Session::flash('flash_pesan','Timesheet sudah dibuat.');
					return redirect('isitimesheet');
	            }
        	}        	
        }        
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
