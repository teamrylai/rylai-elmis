<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

use Illuminate\Http\Request;
use App\Timesheet;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\DB;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use DatePeriod;

use Image;
use Input;
use Validator;
use Redirect;
use Session;
use URL;

class ProfileController extends Controller {


	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$this->denda();
		$user = User::findOrFail(\Auth::user()->nip); //first error
		return view('profile.index', compact('user'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//

	}


	/**
     * This method is used to edit the profile of a user
     * @return show notification that the profile has been updated
     */
	public function edit() 
	{
		$user = \Auth::user();
		return view('profile.edit', compact('user'));
	}

	public function update(Requests\EditProfile $request)
	{
		$input = $request->all();

		$user = User::findOrFail(\Auth::user()->nip);
		if($input['new_password'] != NULL){
			$password = $input['new_password'];
			$pass = \Hash::make($password);
	        //$input['new_password'] = $pass;
			$user->update(['password' => $pass]);
		}
		
		$user->update([
			'nama' => $input['nama'],
			'email' => $input['email'],
			'no_telp' => $input['no_telp']
			]);

		\Session::flash('flash_message', 'Profil telah diubah');

		return redirect('profile');
		
	}


	//meng-update denda terbaru
	public function denda(){

		$currentdenda = Auth::user()->denda;
		$nip = Auth::user()->nip;
		$user = User::find($nip);
		$counter = 0;

		//asumsi denda perhari 20000
		$fine = 20000;
		
		$timesheet = Timesheet::where('nip_pekerja', '=', $user['nip'])->whereBetween('tanggal',[Carbon::now()->startofMonth(),Carbon::now()])->get();
		
		$tsvalid = [];
		foreach ($timesheet as $ts) {
			$dt = $ts['tanggal'];
			$dt2 = Carbon::createFromFormat('Y-m-d', $dt);
			if($dt2->isWeekday()){ $tsvalid[] = $ts; }
		}


		$jumlahngisi = count($tsvalid)*$fine;
		
		$interval = CarbonInterval::days(1);
		$begin  = Carbon::now()->startofMonth();
		$end = Carbon::now();

		$daterange = new DatePeriod($begin, $interval ,$end);

		$tanggal = [];
		foreach( $daterange as $date) 
		{ 
			$dt = $date->format('Y-m-d');
			$dt2 = Carbon::createFromFormat('Y-m-d', $dt);
			if($dt2->isWeekday()){ $tanggal[] = $date; } 

		}

		$jumlahseharusnya = count($tanggal)*$fine;


		
		$denda = $jumlahseharusnya - $jumlahngisi;
		//return $denda;

		
		$user->denda = $denda;
		$user->save();
		
		
		//return $denda;

	}

	public function testmethod(){

		//test return method
		return $this->denda();
	}

	public function uploadSignature() {

		$user = \Auth::user();

	  // getting all of the post data
		$file = array('image' => Input::file('image'));
	  // setting up rules
	  $rules = array('image' => 'required|image|'); //mimes:jpeg,bmp,png and for max size max:10000
	  // doing the validation, passing post data, rules and the messages


	  $validator = Validator::make($file, $rules);
	  
	  if ($validator->fails()) 
	  {
	    // send back to the page with the input data and errors
	  	return Redirect::to('profile')->withInput()->withErrors($validator);
	  }
	  else {
	    // checking file is valid.
	  	if (Input::hasFile('image'))
	  	{

	  		$file = Input::file('image');
	  		$file->move('img/profilepic', urlencode($file->getClientOriginalName()));

	  		$image = Image::make(sprintf('img/profilepic/%s', urlencode($file->getClientOriginalName())))
	  			->resize(200, null, function ($constraint) {
			    $constraint->aspectRatio();
			})->save();



	  		$path = 'img/profilepic/'.urlencode($file->getClientOriginalName());
	  		$user->path = $path;
	  		$user->update();
	      // sending back with message
	  		Session::flash('success', 'Upload successfully'); 
	  		return Redirect::to('profile');
	  	}
	  	else {
	      // sending back with error message.
	  		Session::flash('error', 'uploaded file is not valid');
	  		return Redirect::to('profile');
	  	}
	  }
	}
}
