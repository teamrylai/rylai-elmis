<?php namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\FormCuti;
use App\Form;
use App\User;
use App\Timesheet;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use DateTime;
use DatePeriod;
use DateInterval;
use App\FormLembur;
use Validator;


class FormLemburController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('teamleader', ['only' => ['showFormList', 'approve']]);
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = \Auth::user();
		$forms = Form::where('nip_pekerja', '=', $user->nip)->where('jenis_form', '=', 'lembur')->get();

		
		$calendar = cal_info(0)['months'];
		$begin = Carbon::now()->startOfYear();
		$intervalmonth = new DateInterval('P1M');
		$end = Carbon::now();
		$monthrange = new DatePeriod($begin, $intervalmonth, $end);
		$months = [];
		foreach( $monthrange as $date) { $months[] = $date; }
		$bulan = [];
		foreach( $months as $month ) { 
			$bulan[$month->format('n')] = $calendar[$month->format('n')]; 
		}
		krsort($bulan);

		$jumlahform = [];
		foreach ($bulan as $key => $value) {
			$dt = Carbon::now();
			$dt2 = Carbon::now();
			$dt->month = $key;
			$dt2->month = $key;
			$dt->startOfMonth();
			$dt2->endOfMonth();

			$frms = Form::where('nip_pekerja', '=', $user['nip'])->where('jenis_form', '=', 'lembur')
						->whereBetween('created_at', [ $dt->toDateString(), $dt2->toDateString() ])->get();

			$jumlahform[$key] = count($frms);

		}

		return view('formlembur.listdownload')->with('forms', $forms)->with('bulan', $bulan)->with('jumlahform', $jumlahform);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$user = \Auth::user();
		$projects = $user->project;

		return view('formlembur.create')->with('projects', $projects);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function confirm(Requests\CreateFormLembur $request)
	{
		$input = $request->all();
		$user = \Auth::user();

		/*Form::create([
			'jenis_form' => 'lembur',
			'status' => 'pending',
			'nip_pekerja' => $user['nip'] 
			]);

		$arrayform = Form::orderBy('created_at', 'DESC')->take(1)->get();

		$form = $arrayform[0];

		$formlembur = FormLembur::create([
			'kode' => $form['kode'],
			'jam_mulai' => $input['waktumulai'],
			'jam_selesai' => $input['waktuselesai'],
			'keterangan' => $input['keterangan'],
			]);*/


		flash()->overlay('Form lembur berhasil disimpan', 'Berhasil');

		return redirect('formlembur');
	}

	public function store(Requests\CreateFormLembur $request)
	{
		$user = \Auth::user();

		//Validation rules
        $rules = array(
            'project' => 'required',
			'waktumulai' => 'required',
			'waktuselesai' => 'required',
			'keterangan' => 'required'
            );        
                
        //Validation messages
        $messages = array(
        	'project.required' => 'Project tidak boleh kosong',
            'waktumulai.required' => 'Waktu mulai belum ditentukan',
            'waktuselesai.required' => 'Waktu selesai belum ditentukan',
            'keterangan.required' => 'Harap isi keterangan'            
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        //Validating
        if($validator->fails())
        {                    
            return redirect('formlembur/create')->withErrors($validator)->withInput();
        }
        else
        {
        	Form::create([
			'jenis_form' => 'lembur',
			'status' => 'pending',
			'nip_pekerja' => $user['nip'] 
			]);

			$arrayform = Form::orderBy('created_at', 'DESC')->take(1)->get();

			$form = $arrayform[0];

			$formlembur = FormLembur::create([
				'kode' => $form['kode'],
				'project_id' => $request->project,
				'jam_mulai' => $request->waktumulai,
				'jam_selesai' => $request->waktuselesai,
				'keterangan' => $request->keterangan
				]);


			flash()->overlay('Form lembur berhasil disimpan', 'Berhasil');

			return redirect('formlembur');
        }		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showFormList()
	{
		$user = \Auth::user();
		$users = User::all();
		//$forms = Form::all();
		/*$formlembur = Form::all();
		foreach ( $users as $user ) {
			if ( $user->team_id == \Auth::user()->team_id ) {
				foreach ( $forms as $form ) {
					if ( $user->nip == $form->nip_pekerja && $user->nip != $nip) {
						array_push($formlembur, $form);
					}
				}
			}
		}*/
		$formlembur = Form::where('jenis_form', '=', 'lembur')->get();
		$forms = [];
		foreach ($formlembur as $fl) {
			if($fl->user['team_id']==$user['team_id']){
				$forms[] = $fl;
			}
		}
		//return $forms;
		return view('formlembur.formlist', compact('forms','users'));
		//return $formlembur;
	}//

/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function approve($id)
	{
		//$status = 'approved';
		$formlembur = Form::FindOrFail($id);
		//$formlembur->status = $status;
		
		$approve  = 'approved';
		$pending = 'pending';

		if($formlembur->status == $approve){

			$formlembur->status = $pending;
			$formlembur->save();

			return \Redirect::to('formlembur/formlist')->with('success','Status form lembur telah diubah');
		}

		elseif ($formlembur->status == $pending) {
			$formlembur->status = $approve;
			$formlembur->save();
			
			return \Redirect::to('formlembur/formlist')->with('success','Status form lembur telah diubah');

		}


		
		//return $id;
		//return Redirect('formlembur.formlist'); 
		
	}

	// public function disapprove($id)
	// {
	// 	$status = 'pending';
	// 	$formlembur = Form::FindOrFail($id);
	// 	$formlembur->status = $status;
	// 	$formlembur->save();

	// 	//return $id;
	// 	//return Redirect('formlembur.formlist'); 
	// 	return \Redirect::to('formlembur/formlist')->with('success','Status form lembur telah diubah');
	// }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	public function download($id)
	{
		$user = \Auth::user();

		$dt = Carbon::now();
		$dt2 = Carbon::now();
		$dt->month = $id;
		$dt2->month = $id;
		$dt->startOfMonth();
		$dt2->endOfMonth();

		//return $dt->toDateString();

		$forms = Form::where('nip_pekerja', '=', $user['nip'])->where('jenis_form', '=', 'lembur')
						->whereBetween('created_at', [ $dt->toDateString(), $dt2->toDateString() ])->get();



		$pdf = \App::make('dompdf');


		$pdf = \App::make('dompdf');



		$pdf = $pdf->loadView('formlembur.download', compact('forms', 'user'));

		return $pdf->stream();

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
