<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier {

	//add an array of Routes to skip CSRF check
	private $openRoutes = ['api/auth', 'api/timesheet/daily', 'api/project/user',
							'api/leave', 'api/overtime'];

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

		//skip csrf check
		foreach($this->openRoutes as $route) {

      	if ($request->is($route)) {
        	return $next($request);
	      }
	    }

	    //csrf check
		return parent::handle($request, $next);
	}

}
