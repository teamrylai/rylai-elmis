<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {

	//


    protected $table = 'roles';

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'role_name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    // public function user()
    //  {
    //      return $this->belongsTo('App\User');
    //  }
          

}
