<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RiwayatPekerja extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'riwayat_pekerja';

    protected $primaryKey = 'riwayat_pekerja_id';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array*/
	 
	protected $fillable = ['project_name'];
	
}
