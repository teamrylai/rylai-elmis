<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesan extends Model {

	//
	protected $table = 'pesans';

    protected $primaryKey = 'id';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = ['id_pengirim','subjek', 'isi'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	protected $hidden = [];

	public $timestamps = false;


	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
