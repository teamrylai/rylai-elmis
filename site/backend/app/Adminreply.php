<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Adminreply extends Model {

	//
		//
	protected $table = 'adminreplies';

    protected $primaryKey = 'id';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = ['id_penerima', 'subjek', 'isi'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	protected $hidden = [];

	public $timestamps = false;


	// public function pesan()
	// {
	// 	return $this->belongsTo('App\User');
	// }
}
