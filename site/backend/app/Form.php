<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model {

	//
    protected $table = 'form';

    protected $primaryKey = 'kode';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['jenis_form','status', 'nip_pekerja'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [''];

    public function formcuti()
    {
        return $this->hasOne('App\FormCuti', 'kode', 'kode');
    }
    public function formlembur()
    {
        return $this->hasOne('App\FormLembur', 'kode', 'kode');
    }
    public function user()
    {
        return $this->hasOne('App\User', 'nip', 'nip_pekerja');
    }
        
}
