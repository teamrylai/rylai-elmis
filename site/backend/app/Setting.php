<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model {

	//


	protected $table = 'settings';


    protected $primaryKey = 'set_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'email'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;
}
