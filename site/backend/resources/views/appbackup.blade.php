<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>@yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
    <!--external css-->
    <link href="{{ asset('/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="{{ asset('/css/zabuto_calendar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/gritter/css/jquery.gritter.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/lineicons/style.css') }}"> 
    <link href="{{ asset('/js/fullcalendar/bootstrap-fullcalendar.css') }}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link href="{{ asset('/js/fancybox/jquery.fancybox.css') }}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/jquery-ui.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/jquery-ui.structure.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/jquery-ui.theme.css') }}">
    
    <!-- Custom styles for this template -->
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/dataTables.bootstrap.css') }}" rel="stylesheet">


    <script src="{{ asset('/js/chart-master/Chart.js') }}"></script>

    @yield('headscript')
    

    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="{!! URL::to('home') !!}" class="logo"><b style="color:#424a5d">ELMIS</b></a>
            <!--logo end-->
            <div class="top-menu">
              <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="{!! URL::to('auth/logout') !!}">Logout</a></li>
              </ul>
            </div>
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      
        <!--sidebar start-->
        <aside>
            <div id="sidebar"  class="nav-collapse ">
                <!-- sidebar menu start-->
                <ul class="sidebar-menu" id="nav-accordion">
                
                    <p class="centered"><a href="{!! URL::to('profile') !!}"><img src="{{ asset('/img/ui-sam.jpg') }}" class="img-circle" width="60"></a></p>
                    <h5 class="centered">{{ \Auth::user()->username }}</h5>
                    <p class="centered" style="color:white">{{ \Auth::user()->role->role_name }}</p>
                    
                    <li class="mt" >
                        <a class="{!! Request::path() == 'home' ? 'active' : ''; !!}" href="{!! URL::to('home') !!}">
                            <i class="fa fa-home"></i>
                            <span>Home</span>
                        </a>
                    </li>

                    <li class="sub-menu">
                        <a class="{!! Request::path() == 'isitimesheet' ? 'active' : ''; !!}" href="{!! URL::to('isitimesheet') !!}" >
                            <i class="fa fa-calendar"></i>

                            <span>Timesheet</span>
                        </a>
                    </li>

                    <li class="sub-menu">
                        <a {!! Request::is('rekap') ? 'class="active"' : null !!} href="{!! URL::to('rekap') !!}" >
                            <i class="fa fa-table"></i>
                            <span>Rekap</span>
                        </a>
                    </li>

                     @if(\Auth::user()->role_id == '2')
                    <li class="sub-menu">
                        <a {!! Request::is('formcuti') ? 'class="active"' : Request::is('formlembur') ? 'class="active"' : null !!} href="" >
                            <i class="fa fa-list-alt"></i><span>Form</span>
                        </a>
                        <ul class="sub">
                            <li {!! Request::is('formcuti') ? ' class="active"' : null !!}><a  href="{!! URL::to('formcuti') !!}">Cuti</a></li>
                            <li {!! Request::is('formlembur') ? ' class="active"' : null !!}><a  href="{!! URL::to('formlembur') !!}">Lembur</a></li>
                        </ul>
                    </li>
                    @endif

                     @if(\Auth::user()->role_id == '1' ||  \Auth::user()->role_id == '3')
                    <li class="sub-menu">
                        <a {!! Request::is('formcuti') ? 'class="active"' : Request::is('formlembur') ? 'class="active"' : null !!} href="" >
                            <i class="fa fa-list-alt"></i><span>Form</span>
                        </a>
                        <ul class="sub">
                            <li {!! Request::is('formcuti') ? ' class="active"' : null !!}><a  href="{!! URL::to('formcuti') !!}">Cuti</a></li>
                            <li {!! Request::is('formlembur') ? ' class="active"' : null !!}><a  href="{!! URL::to('formlembur') !!}">Lembur</a></li>
                        </ul>
                    </li>
                    @endif
                    @if(\Auth::user()->role_id == '1')
                    <li class="sub-menu">
                        <a {!! Request::is('riwayatpekerja*') ? 'class="active"' : Request::is('riwayatproyek*') ? 'class="active"' : null !!} href="" >
                            <i class="fa fa-history"></i>
                            <span>Riwayat</span>
                        </a>
                        <ul class="sub">
                            <li {!! Request::is('riwayatpekerja*') ? ' class="active"' : null !!}><a  href="{!! URL::to('riwayatpekerja') !!}">Pekerja</a></li>
                            <li {!! Request::is('riwayatproyek*') ? ' class="active"' : null !!}><a  href="{!! URL::to('riwayatproyek') !!}">Proyek</a></li>
                        </ul>
                    </li>
                    @endif
                    @if(\Auth::user()->role_id == '2' || \Auth::user()->role_id == '3')
                    <li class="sub-menu">
                        <a {!! Request::is('riwayatpekerja*') ? 'class="active"' : Request::is('riwayatproyek') ? 'class="active"' : null !!} href="{!! URL::to('riwayatpekerja') !!}/{!! \Auth::user()->nip !!}" >
                            <i class="fa fa-history"></i>
                            <span>Riwayat</span>
                        </a>
                    </li>
                    @endif

                    @if(\Auth::user()->role_id == '1')
                    <li class="sub-menu">
                        <a {!! Request::is('statistik*') ? 'class="active"' : null !!} href="{!! URL::to('statistik') !!}"  >
                            <i class="fa fa-bar-chart"></i>
                            <span>Statistik</span>
                        </a>
                    </li>
                    @endif


                    <li class="sub-menu">
                        <a {!! Request::is('pesan*') ? ' class="active"' : null !!} href="{!! URL::to('pesan') !!}" >
                            <i class="fa fa-comments"></i>
                            <span>Pesan</span>
                        </a>
                    </li>
                    @if(\Auth::user()->role_id == '1')
                    <li class="sub-menu">
                        <a {!! Request::is('user*') ? 'class="active"' : Request::is('email*') ? 'class="active"' : null !!} href="" >
                            <i class="fa fa-wrench"></i>
                            <span>Pengaturan</span>
                        </a>
                        <ul class="sub">

                            <li {!! Request::is('user*') ? 'class="active"' : null !!}><a  href="{!! URL::to('user') !!}">User</a></li>
                            <li {!! Request::is('email*') ? 'class="active"' : null !!}><a  href="{!! URL::to('email') !!}">Email</a></li>
                        </ul>
                    </li>
                    @endif

                </ul>
                <!-- sidebar menu end-->
            </div>
        </aside>
        <!--sidebar end-->
      
      
     <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
            {{-- <h3><i class="fa fa-angle-right"></i> Blank Page</h3> --}}
            {{-- <div class="row mt"> --}}
              <div class="col-lg-12">
              @include ('flash::message')
              @yield('content')
              </div>
            {{-- </div> --}}
          </section>
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{{ asset('/js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-ui.js') }}"></script>

    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <script class="include" type="text/javascript" src="{{ asset('/js/jquery.dcjqaccordion.2.7.js') }}"></script>
    <script src="{{ asset('/js/jquery.scrollTo.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.nicescroll.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/js/jquery.sparkline.js') }}"></script>
    



    <!--common script for all pages-->
    <script src="{{ asset('/js/common-scripts.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/js/gritter/js/jquery.gritter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/gritter-conf.js') }}"></script>

    <!--script for this page-->
    <script src="{{ asset('/js/sparkline-chart.js') }}"></script>    
  <script src="{{ asset('/js/zabuto_calendar.js') }}"></script>
    <script src="{{ asset('/js/chart-master/Chart.js') }}"></script>
    <script src="{{ asset('/js/chartjs-conf.js') }}"></script>

  <!--custom switch-->
  <script src="{{ asset('/js/bootstrap-switch.js') }}"></script>
  
  <!--custom tagsinput-->
  <script src="{{ asset('/js/jquery.tagsinput.js') }}"></script>
  
  <!--custom checkbox & radio-->
  
  <script type="text/javascript" src="{{ asset('/js/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/js/bootstrap-daterangepicker/date.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  
  <script type="text/javascript" src="{{ asset('/js/bootstrap-inputmask/bootstrap-inputmask.min.js') }}"></script>



  
  
  <script src="{{ asset('/js/form-component.js') }}"></script>    


    
    
  <script>
      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>  
  
  
  <script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
      @yield('script')

      <script>
          $('#flash-overlay-modal').modal();
      </script>
<script>
$('.datepicker').datepicker();
</script>
  

  </body>
</html>
