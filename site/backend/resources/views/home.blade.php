@extends('app')

@section('title')
Home
@stop

@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap-slider.css')}}">
@stop
@section('content')
	
	<div class="row">
	Selamat datang {{\Auth::user()->nama}}!
	<div style="float:right;">
		{!!$tanggal->formatLocalized('%A %d %B %Y');!!}
	</div>
	</div>
	<div class="row" align="center">
	@if($tsheethariini->count() > 0)
	<h5><span class="glyphicon glyphicon-ok"></span>TIMESHEET</h5>

	@else
	<span class="glyphicon glyphicon-remove"></span>TIMESHEET<br>
			<label style="font-size:11px; color:black;">
			*silahkan isi timesheet untuk memenuhi rekap hari ini
			</label>
	<br>
	<a href="isitimesheet"><button class="btn btn-primary"><span class="glyphicon glyphicon-calendar"></span>ISI TIMESHEET!</button></a>
	@endif
	</div>
	<hr>
	
	<div class="row" align="center">
		<span class="btn btn-danger" disabled style="background-color:red;">Denda bulan ini: Rp. {{$denda}}</span>
		<br><br>
	</div>
	<div class="row">
		<div class="col-md-6 well">												
			<h3>Timesheet yang belum diisi bulan ini:</h3>
			<div class="table-responsive">
				<table class="table table-fixed table-striped">
					<thead>
						<tr>
							<th style="text-align:center;">Tanggal</th>
							<th style="text-align:center;">Isi Timesheet</th>
						</tr>
					</thead>
					<tbody>
						@foreach($missedtimesheet as $ms)
						<tr>
							<th style="text-align:center;">{{ $ms->toFormattedDateString() }}</th>
							<th style="text-align:center;">
							<div><a href="#myModal" data-toggle="modal" id="{{$ms->toDateString()}}" data-target="#timesheet-modal"><span class="label label-primary label-mini"><span class="glyphicon glyphicon-plus"></span></span></a></div>
							</th>
						</tr>
						@endforeach
					</tbody>
				</table>					
			</div>

			{{-- Modal -- Form TimeSheet Dialog --}}
			<div id="timesheet-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                {!! Form::open(array('action' => 'HomeController@store')) !!}
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel">ISI TIMESHEET <span id="misseddatespan"></span></h4>
                  </div>
                  <div class="modal-body">                  
                  	<div class="row">
                  		<div class="col-sm-12">
                  		<input type="hidden" name="tanggal" class="form-control" id="misseddate"/>
                  			<div class="form-group col-sm-6">                  				
                  				<div id="proyek-field" class="form-group">
									<select name="project" class="form-control" id="proyek">
										@foreach ($projectuser as $project)
										<option value="{{ $project['project_id'] }}">{{ $project['project_name'] }}</option>
										@endforeach
									</select>
								</div>
                  			</div>
                  			<div class="form-group col-sm-6">
								<select name="status_absensi" class="form-control" id="status_absensi">
									<option value="H">Hadir</option>
									<option value="S">Sakit</option>
									<option value="W">Work From Home</option>
									<option value="D">Dinas Luar</option>
								</select>
							</div>
                  		</div>

                  		<div class="col-sm-12">
                  			<div class="form-group">
					            <label>Pukul: </label>
					            <input type="text" name="waktumulai" id="waktumulai" value="08:00" style="border:none; color: #666666; text-align:center" size="7" readonly/>
					            <label>-</label>
								<input type="text" name="waktuselesai" id="waktuselesai" value="18:00" style="border:none; color: #666666; text-align:center" size="7" readonly/><br>
								<input type="text" class="slider" id="sl1" name="q12" value="" data-slider-min="480" data-slider-max="1080" data-slider-step="5" data-slider-value="[480,1080]" data-slider-orientation="horizontal" data-slider-selection="after" data-slider-tooltip="hide">			          
	         				</div>
                  		</div>
                  		<div class="col-sm-12">
                  			<div class="form-group">
	                  			<label>Kegiatan: </label>
	                  			<div id="kegiatan-field" class="form-group {{ $errors->has('kegiatan') ? 'has-error' : '' }}">
						    	{!! Form::textarea('kegiatan', null, ['class'=>'form-control', 'id' => 'kegiatan', 'placeholder' => 'Kegiatan']) !!}
						    	{!! $errors->first('kegiatan', '<span class="help-block">:message</span>') !!}
						    	</div>
						    </div>	
                  		</div>
                  		
                  	</div>
                  </div>
                  <div class="modal-footer">
				    <div class="form-group form-inline text-center"> 
				    	<button type="submit" class="btn btn-primary form-control" id="submitButton"><i class="fa fa-check"></i> Simpan</button>
				    </div>
                  </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>

		{{-- script untuk ambil tanggal dari daftar timesheet yang terlewatkan --}}
		<script>
		$('#timesheet-modal').on('show.bs.modal', function(e) {
		    
		    var $modal = $(this),
		        esseyId = e.relatedTarget.id;
		      $modal.find('#misseddate').val(esseyId);
		      $modal.find('#misseddatespan').html(esseyId);

		    
		})
		</script>
		</script>
		</div>
		<div class="col-md-6">
		<div class="col-md well">
			<h3>Tabel kegiatan</h3>
				<div class="table-responsive">
						<table class="table table-fixed table-striped">
						<thead>
							<tr>
								<th style="text-align:center;">Nama Proyek</th>
								<th style="text-align:center;">Kegiatan</th>
								<th style="text-align:center;">Pukul</th>
							</tr>
						</thead>
						<tbody>

							@foreach($tsheethariini as $ts)
							@if($projecthariini = \App\Project::where('project_id',  $ts['project_id'])->get())
							@endif
							<tr style="text-align:center;">
								<td>@foreach($projecthariini as $p) {{$p['project_name']}} @endforeach</td>
								<td>{{ $ts['kegiatan']}}</td>
								<td>{{ $ts['waktu_mulai']." - ".$ts['waktu_selesai']}}</td>
							</tr>
							@endforeach
						</tbody>
					</table>					
				</div>
			<a href="isitimesheet"><button class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>TAMBAH KEGIATAN</button></a>					
		</div>
		
		<div class="col-md well">
			<h3>Rekap hari ini</h3>
				<div class="table-responsive">
						<table class="table table-fixed table-striped">
						<thead>
							<tr>
								<th style="text-align:center;">Absensi</th>
								<th style="text-align:center;">Timesheet</th>
							</tr>
						</thead>
						<tbody>
							<tr style="text-align:center;">
							@forelse($tsheethariini->take(1) as $ts)

								@if($ts['status_absensi'] == 'H')
								<td align="center"><span class="label label-success label-mini">{{ $ts['status_absensi'] }}</td>
								@elseif($ts['status_absensi'] == 'C')
								<td><span class="label label-default label-mini">{{ $ts['status_absensi'] }}</td>
								@elseif($ts['status_absensi'] == 'W')
								<td><span class="label label-primary label-mini">{{ $ts['status_absensi'] }}</td>
								@elseif($ts['status_absensi'] == 'D')
								<td><span class="label label-info label-mini">{{ $ts['status_absensi'] }}</td>
								@elseif($ts['status_absensi'] == 'S')
								<td><span class="label label-default label-mini">{{ $ts['status_absensi'] }}</td>
								@else
								<td>{{ $ts['status_absensi'] }}</td>
								@endif

								@if($ts['status_isian'] == 'OK' && $ts['created_at']->format('Y-m-d') > $ts['tanggal'])
								<td style="background-color:#f0ad4e; min-width:27px;" align="center	"><span class="label label-warning label-mini">{{ $ts['status_isian'] }}</td>
								@elseif($ts['status_isian'] == 'OK' && $ts['created_at']->format('Y-m-d') == $ts['tanggal'])
								<td style="background-color:#5cb85c; min-width:27px" align="center"><span class="label label-success label-mini">{{ $ts['status_isian'] }}</td>
								@elseif($ts['status_isian'] == 'C')
								<td><span class="label label-default">{{ $ts['status_isian'] }}</td>
								@elseif($ts['status_isian'] == 'S')
								<td><span class="label label-default">{{ $ts['status_isian'] }}</td>
								@else
								<td style="font-weight:bold; color:red;">{{ $ts['status_isian'] }}</td>			
								@endif
							@empty
							<td style="min-width:27px">----</td>
							<td style="min-width:27px">----</td>
							@endforelse
							</tr>
						</tbody>
					</table>					
				</div>			
		</div>
		
		</div>			
	</div>	


			
			

			
		


	

@stop

