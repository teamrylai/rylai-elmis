<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="description" content="UI Kit.">
  <meta name="author" content="Faizal Rahman">

  <link rel="apple-touch-icon" sizes="57x57" href="{{url('img/favicon/apple-icon-57x57.png')}}">
  <link rel="apple-touch-icon" sizes="60x60" href="{{url('img/favicon/apple-icon-60x60.png')}}">
  <link rel="apple-touch-icon" sizes="72x72" href="{{url('img/favicon/apple-icon-72x72.png')}}">
  <link rel="apple-touch-icon" sizes="76x76" href="{{url('img/favicon/apple-icon-76x76.png')}}">
  <link rel="apple-touch-icon" sizes="114x114" href="{{url('img/favicon/apple-icon-114x114.png')}}">
  <link rel="apple-touch-icon" sizes="120x120" href="{{url('img/favicon/apple-icon-120x120.png')}}">
  <link rel="apple-touch-icon" sizes="144x144" href="{{url('img/favicon/apple-icon-144x144.png')}}">
  <link rel="apple-touch-icon" sizes="152x152" href="{{url('img/favicon/apple-icon-152x152.png')}}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{url('img/favicon/apple-icon-180x180.png')}}">
  <link rel="icon" type="image/png" sizes="192x192"  href="{{url('img/favicon/android-icon-192x192.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{url('img/favicon/favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="96x96" href="{{url('img/favicon/favicon-96x96.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{url('img/favicon/favicon-16x16.png')}}">
  <link rel="manifest" href="{{url('img/favicon/manifest.json')}}">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">

  <meta name="viewport" content="width=device-width,initial-scale=1">


    <title>ELMIS - Log Management</title>

    <!--fonts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/fonts/font-text/fonts.css')}}">
    <link rel="stylesheet" href="{{ asset('/font-awesome/css/font-awesome.min.css') }}">
    
    <!--bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap-select.css')}}">  
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap-theme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap-datetimepicker.css')}}">

    <!-- CSS Component -->
    <link rel="stylesheet" href="{{url('css/component.css')}}">  
  </head>

  <body>      
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">
            <img class="icon-menu" src="{{url('img/Logo.png')}}">
          </a>
          <a class="navbar-brand title" href="{{url('home')}}">
            ELMIS
          </a>
        </div>
      </div>
    </nav>
    
    <section id="content">
    <div class="container">
      <h1>ELMIS</h1>
      <h2>Employee<br>Log Management</h2>
      <div class="row margin">
        <div class="col-md-8">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <strong>Whoops!</strong> There were some problems with your input.<br><br>
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          @if(Session::has('message'))
            <p class="alert-danger">{{Session::get('message')}}</p>
          @endif
          <form role="form" method="POST" action="{{ url('/auth/login') }}" class="form-inline">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}">
            <input type="password" class="form-control" placeholder="Password" name="password">
            <button class="btn btn-secondary">Login</button>
          </form>
        </div>
      </div>      
    </div>
    </section>    
  </body>
</html>
