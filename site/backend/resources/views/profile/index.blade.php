@extends('app')

@section('title')
Profil  {{ $user->nama }}
@stop

@section('content')

<h1>> Profile Saya</h1>

<div class="content-panel">

  <h1> <strong>{{ $user->nama }}</strong> ({{ $user->username }})</h1>
  <table class="table">
    <tbody>
      <tr><th class="col-md-3 text-right">NIP:</th><td class="col-md-9">{{ $user->nip }}</td></tr>
      <tr><th class="text-right">Email:</th><td>{{ $user->email }}</td></tr>
      <tr><th class="text-right">Nomor Telepon:</th><td>{{ $user->no_telp }}</td></tr>
      <tr><th class="text-right">Role:</th><td>{{ $user->role->role_name }}</td></tr>
      <tr><th class="text-right">Jabatan:</th><td>{{ $user->jabatan }}</td></tr>
      <tr>
        <th class="text-right">Team:</th>
        <td>
        @if($user->team_id != null)
          {{ $user->team->team_name }}
        @endif
        </td>
      </tr>
      <tr><th class="text-right">Denda bulan ini:</th><td>Rp {{ $user->denda }},-</td></tr>
      <tr>
        <th class="text-right">
          My Signature :
        </th>
        <td>
          
          <div class="form-group">
          @if($user->path != NULL)
          <img id="digsign" src="{{ asset($user->path) }}" style="border-style:solid">
          @else
          Upload your signature now
          @endif
          </div>          
          
            {!! Form::open(array('url'=>'profile/uploadSignature','method'=>'POST', 'files'=>true)) !!}
            {!! Form::file('image', array('class'=>'form-control btn btn-xs')) !!}
            <p class="errors">{!!$errors->first('image')!!}</p>
            @if(Session::has('error'))
            <p class="errors">{!! Session::get('error') !!}</p>
            @endif
            <div id="success"> </div>
            {!! Form::submit('Upload', array('class'=>'btn btn-xs')) !!}
            {!! Form::close() !!}
        </td>
      </tr>
    </tbody>
  </table>
  <div class="form-inline text-center"><a href="{{ action('ProfileController@edit') }}" type="button" class="form-control btn btn-primary">Edit Profil </a></div>

</div>





@stop

@section('footer')