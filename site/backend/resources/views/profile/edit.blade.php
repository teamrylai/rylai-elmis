@extends('app')

@section('title')
Edit Profile {{ $user->nama }}
@stop

@section('content')
<h1>> <a href="{{ action('ProfileController@index') }}">Profile Saya </a>> Edit</h1>
<div class="form-panel">


    {!! Form::model($user, ['method' => 'PATCH', 'action' => 'ProfileController@update', 'id' => 'editProfile' ]) !!}


    <!-- Nama Form Input -->
        
        <div class="form-group">
            {!! Form::label('Nama', 'Nama:') !!}
            {!! Form::text('nama', null, ['class'=>'form-control']) !!}
            {!! $errors->first('nama', '<span class="help-block">:message</span>') !!}
        </div>

        <!-- Email Form Input -->
        
        <div class="form-group">
            {!! Form::label('Email', 'Email:') !!}
            {!! Form::text('email', null, ['class'=>'form-control']) !!}
            {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
        </div>


    <!-- no_telp Form Input -->
    <div class="form-group">
        {!! Form::label('no_telp', 'No. Telp:') !!}
        {!! Form::text('no_telp', null, ['class'=>'form-control']) !!}
        {!! $errors->first('no_telp', '<span class="help-block">:message</span>') !!}
    </div>
    

    <!-- New Password Form Input -->

    <div class="form-group">
        {!! Form::label('New Password', 'New Password:') !!}
        {!! Form::password('new_password', ['class'=>'form-control']) !!}
    </div>

    <!-- Confirm Password Form Input -->
    <div class="form-group">
        {!! Form::label('Confirm Password', 'Confirm Password:') !!}
        {!! Form::password('confirm_password', ['class'=>'form-control']) !!}
        {!! $errors->first('confirm_password', '<span class="help-block">:message</span>') !!}
    </div>

    <div class="form-group form-inline text-center">
        {!! Form::submit('Ubah', ['class' => 'btn btn-primary form-control', 'id' => 'submitButton'])  !!}
    </div>

    {!! Form::close() !!}
</div>



@stop

@section('script')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script>
$(document).on("click", "#submitButton", function(e) {
    e.preventDefault();
    bootbox.confirm("Anda yakin ingin menyimpan perubahan ini?", function(result) {
        if (result) {
            $('#editProfile').submit();
        }
    });
});
</script>
@stop
