@extends('app')

@section('title')
Isi Timesheet
@stop

@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap-slider.css')}}">
@stop

	@if(Session::has('flash_pesan'))
    <div class=" alert alert-success">{{Session::get('flash_pesan')}}</div>
    @endif

@section('content')


<div class="col-md-6 col-md-offset-3">
<h1>Isi Timesheet</h1>
<hr>
	<div class="form-panel">
	{!! Form::open(['url' => 'isitimesheet', 'id' => 'timesheet']) !!}
	<!-- Waktu Form Input -->
		<div class="form-inline form-group {{ $errors->has('tanggal') ? 'has-error' : '' }}">
			<div class="form-group">
              <div class='input-group date'>
                <span class="input-group-addon">
                    <button class="btn btn-success" type="button">
                      <span class="glyphicon glyphicon-calendar"></span> PICK A DATE!
                    </button>
                </span>
                <input type='text' name="tanggal" class="form-control" value="{{$date}}" />
              </div>
          	</div>

			<div class="form-group">
				<select name="status_absensi" class="form-control" id="status_absensi">
					<option value="H">Hadir</option>
					<option value="S">Sakit</option>
					<option value="W">Work From Home</option>
					<option value="D">Dinas Luar</option>
				</select>
			</div>
			{!! $errors->first('tanggal', '<span class="help-block">:message</span>') !!}
		</div>

		<div id="proyek-field" class="form-group">
			{!! Form::label('Proyek', 'Proyek:', ['id'=>'labelproyek']) !!}
			@if(!$projectuser->isEmpty())
			<select name="project" class="form-control" id="proyek">
				@foreach ($projectuser as $project)
				<option value="{{ $project['project_id'] }}">{{ $project['project_name'] }}</option>
				@endforeach
			</select>
			@else
			<p>No project assigned to you right now. Please <a href="pesan/create">contact</a> admin</p>
			@endif
		</div>		

         <div class="form-inline form-group">
			<div class="form-group">
              <label>Durasi</label> 
          	</div>
			<div class="form-group">
				<input type="number" name="jam" value="0" id="jam" class="form-control" min="0" max="23"/>
			</div>
			<div class="form-group">
				Jam
			</div>
			<div class="form-group">
				<input type="number" name="menit" value="0" step="5"  id="menit" class="form-control" min="0" max="59"/>
			</div>
			<div class="form-group">
				Menit
			</div>
		</div>
         

		<div id="kegiatan-field" class="form-group {{ $errors->has('kegiatan') ? 'has-error' : '' }}">
	    <label>Kegiatan: </label>
	    	{!! Form::textarea('kegiatan', null, ['class'=>'form-control', 'id' => 'kegiatan', 'placeholder' => 'Kegiatan']) !!}
	    	{!! $errors->first('kegiatan', '<span class="help-block">:message</span>') !!}
	    </div>
	    
	    <div class="form-group form-inline text-center"> 
	    	

	    	<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-check"></i> Simpan</button>
	    </div>
	    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel">Are you sure?</h4>
                  </div>

                  <div class="modal-footer">
                  	<button type="submit" class="btn btn-success" id="submitButton"><i class="fa fa-check"></i> YES</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>

                </div>
              </div>
            </div>
    

	</div>



	{!! Form::close() !!}		
           
</div>
@stop



		@section('script')
		<script src="{{ asset('/js/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
		<script type="text/javascript">

		</script>

		<script type="text/javascript">
		$("select#status_absensi").change(function(){
			if(document.getElementById("status_absensi").value == 'S'){
				document.getElementById('proyek-field').className = 'hidden';				
				document.getElementById('kegiatan-field').className = 'hidden';
				document.getElementById('kegiatan').value = 'Sakit';
			}
			else{
				document.getElementById('proyek-field').className = 'show form-group';				
				document.getElementById('kegiatan-field').className = 'show form-group';
				document.getElementById('kegiatan').value = '';
			}
		});
		</script>

		
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script>
$(document).on("click", "#submitButton", function(e) {
	e.preventDefault();
	bootbox.confirm("Anda yakin ingin menyimpan timesheet ini?", function(result) {
		if (result) {
			$('#timesheet').submit();
		}
	});
});
</script>

@stop