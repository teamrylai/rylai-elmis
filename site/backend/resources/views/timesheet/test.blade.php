@extends('app')

@section('title')
Isi Timesheet 2
@stop

@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap-slider.css')}}">
@stop

@section('content')
<div class="col-md-6 col-md-offset-3">

	<input type="text" class="slider" id="sl1" name="q12" value="" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="50" data-slider-orientation="horizontal" data-slider-selection="after" data-slider-tooltip="show">
</div>

<button id="getVal" class="btn">Get Values</button>

<script>
	$(function(){
    $('#sl1').slider({
          formater: function(value) {
            return 'Current value: '+value;
          }
    });
    $('#sl2').slider({
          formater: function(value) {
            return 'Current value: '+value;
          }
    });
    $('#sl3').slider({
          formater: function(value) {
            return 'Current value: '+value;
          }
    });
    
    $("#getVal").click(function() {
          alert($("#sl1").data('slider').getValue());
 	});
});
</script>
@stop