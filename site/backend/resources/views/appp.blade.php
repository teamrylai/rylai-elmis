<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	  <title>@yield('title')</title>
	  <meta name="description" content="UI Kit.">
	  <meta name="author" content="Faizal Rahman">

	  <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('/img/favicon/apple-icon-57x57.png') }}">
	  <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('/img/favicon/apple-icon-60x60.png') }}">
	  <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('/img/favicon/apple-icon-72x72.png') }}">
	  <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
	  <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
	  <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
	  <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
	  <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
	  <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
	  <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
	  <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
	  <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
	  <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
	  <link rel="manifest" href="{{ asset('/img/favicon/manifest.json') }}">
	  <meta name="msapplication-TileColor" content="#ffffff">
	  <meta name="msapplication-TileImage" content="{{ asset('/img/favicon/ms-icon-144x144.png') }}">
	  <meta name="theme-color" content="#ffffff">

	  <meta name="viewport" content="width=device-width,initial-scale=1">

	  <!-- Font Awesome -->
	  <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" />

	  <!-- Fonts -->
	  <link rel="stylesheet" href="{{ asset('/fonts/font-text/fonts.css') }}">

	  <!-- CSS Bootstrap -->
	  <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}">
	  <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap-select.css') }}">
	  <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap-slider.css') }}">
	  <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap-theme.css') }}">
	  <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap-datetimepicker.css') }}">

	  <!-- Custom styles for this template -->
	    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
	    <link href="{{ asset('/css/style-responsive.css') }}" rel="stylesheet">

	  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	  <script type="text/javascript" src="{{ asset('/js/accordion.js') }}"></script>

	  <!-- CSS Component -->
	  <link rel="stylesheet" href="{{ asset('/css/component.css') }}">
</head>
<body>
	<section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="index.html" class="logo"><b>ELMIS</b></a>
            <!--logo end-->
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="auth/logout">Logout</a></li>
            	</ul>
            </div>
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
                  
                  @if(\Auth::user())
                    <p class="centered"><a href="profile">Welcome, {{ \Auth::user()->username }}</a></p>
                  @endif
              	  
              	  <h5 class="centered"><a href=""></a></h5>
              	  	
                  <li class="mt" >
                      <a class="{!! Request::path() == 'home' ? 'active' : ''; !!}" href="{!! URL::to('home') !!}">
                          <i class="fa fa-home"></i>
                          <span>Home</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="{!! URL::to('timesheet') !!}" >
                          <i class="fa fa-calendar"></i>

                          <span>Timesheet</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="{!! URL::to('rekap') !!}" >
                          <i class="fa fa-table"></i>
                          <span>Rekap</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a {!! Request::is('formcuti') ? 'class="active"' : Request::is('formlembur') ? 'class="active"' : null !!} href="" >
                          <i class="fa fa-list-alt"></i><span>Form</span>
                      </a>
                      <ul class="sub">
                          <li {!! Request::is('formcuti') ? ' class="active"' : null !!}><a  href="{!! URL::to('formcuti') !!}">Cuti</a></li>
                          <li><a  href="{!! URL::to('formlembur') !!}">Lembur</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a {!! Request::is('riwayatpekerja') ? 'class="active"' : Request::is('riwayatproyek') ? 'class="active"' : null !!} href="" >
                          {{-- <i class="fa fa-history"></i> --}}
                          <span>Riwayat</span>
                      </a>
                      <ul class="sub">
                          <li {!! Request::is('riwayatpekerja') ? ' class="active"' : null !!}><a  href="{!! URL::to('riwayatpekerja') !!}">Pekerja</a></li>
                          <li {!! Request::is('riwayatproyek') ? ' class="active"' : null !!}><a  href="{!! URL::to('riwayatproyek') !!}">Proyek</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="{!! URL::to('pesan') !!}" >
                          <i class="fa fa-comments"></i>
                          <span>Pesan</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a href="" >
                          <i class="fa fa-wrench"></i>
                          <span>Pengaturan</span>
                      </a>
                      <ul class="sub">

                          <li><a  href="{!! URL::to('user') !!}"><i class="fa fa-users"></i>User</a></li>
                          <li><a  href="{!! URL::to('email') !!}">Email</a></li>
                      </ul>
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      **************************************************************************************************************  ********************************************* -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">

            
            @include('flash::message')
          	@yield('content')

            

            
          </section>
      </section>
  </section>

    <!-- jQuery -->
	  <script type="text/javascript" src="{{ asset('/js/jquery-1.11.1.min.js') }}"></script>
	  <script type="text/javascript" src="{{ asset('/js/bootstrap.js') }}"></script>
	  <script type="text/javascript" src="{{ asset('/js/bootstrap-select.js') }}"></script>
	  <script type="text/javascript" src="{{ asset('/js/bootstrap-slider.min.js') }}"></script>
	  <script type="text/javascript" src="{{ asset('/js/bootstrap.file-input.js') }}"></script>
	  <script type="text/javascript" src="{{ asset('/js/moment-with-locales.js') }}"></script>
	  <script type="text/javascript" src="{{ asset('/js/bootstrap-datetimepicker.min.js') }}"></script>
	  <script type="text/javascript" src="{{ asset('/js/main.js') }}"></script>

	  <script src="{{ asset('/js/jquery.js') }}"></script>
    <script src="{{ asset('/js/jquery-1.8.3.min.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <script class="include" type="text/javascript" src="{{ asset('/js/jquery.dcjqaccordion.2.7.js') }}"></script>
    <script src="{{ asset('/js/jquery.scrollTo.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.nicescroll.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/js/jquery.sparkline.js') }}"></script>


    <!--common script for all pages-->
    <script src="{{ asset('/js/common-scripts.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/js/gritter/js/jquery.gritter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/gritter-conf.js') }}"></script>

    <!--script for this page-->
    <script src="{{ asset('/js/sparkline-chart.js') }}"></script>    
	<script src="{{ asset('/js/zabuto_calendar.js') }}"></script>	
	  
    <script>
      $('#flash-overlay-modal').modal();
     // $('div.alert').not('alert-important').delay(3000).slideUp(300);
    </script>

    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
</body>
</html>
