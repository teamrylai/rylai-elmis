@extends('app')

@section('title')
	Email
@stop

@section('content')

<div class="row">
    <div class="col-md-6">
        
    <h1>Notifikasi Rekap</h1>
    Hanya user yang dipilih yang akan mendapatkan e-mail rekap timesheet
    <!-- {!! Form::open(['url' => 'emailrekap', 'id' => 'settings2']) !!} -->
        
        {!! Form::open(['url' => 'emailrekap', 'id' => 'settings2']) !!}
        
    <br>
                
        {!! Form::checkbox('checkall',null, false, ['id'=>'trigger', 'class'=>'checkbox']) !!} 
        <span class="lbl padding-8">Pilih Semua</span>

        @foreach ($users as $user)
        <br>

        <!-- {!! Form::checkbox('email', $user->email) !!} {{ $user->nama }} ({{ $user->email }})  -->
        {!! Form::checkbox('emails[]', $user->email,false,['class'=>'checkbox']) !!} 
        <span class="lbl padding-8"> {{ $user->nama }} ({{ $user->email }}) </span>

        @endforeach
        
        
        <br>

        <!-- {!! Form::submit('Save') !!} -->
        <br>
        
            <button type="submit" class="btn btn-primary" id="tombolSubmit"><i class="fa fa-check"></i> Save settings</button>
        

    {!! Form::close() !!}
    </div>
    <div class="col-md-6">
        <h1> Timesheet Reminder </h1>

    
    {!! HTML::ul($errors->all()) !!}
    {!! Form::open(['url' => 'emailts', 'id' => 'settings']) !!}
    



            <div class="form-group">
            Reminder pengisian timesheet akan dikirimkan setiap hari pada jam
       
            {!! Form::select('jam_notif', [null=>'____','15:00:00'=>'15:00:00','16:00:00'=>'16:00:00','17:00:00'=>'17:00:00','18:00:00'=>'18:00:00',
                 '19:00:00'=>'19:00:00', '20:00:00'=>'20:00:00','21:00:00'=>'21:00:00','22:00:00'=>'22:00:00',
                 '23:00:00'=>'23:00:00']) !!} WIB.
                <br><br><button type="submit" class="btn btn-primary" id="submitButton"><i class="fa fa-check"></i> Save settings</button>
            </div>
            
                
            
        <br>
    {!! Form::close() !!}
    </div>
</div>

	


@stop

@section('script')
    <script src="{{ asset('/js/bootbox.min.js') }}"></script>
    <script>
        $(document).on("click", "#submitButton", function(e) {
            e.preventDefault();
            bootbox.confirm("Anda yakin ingin menyimpan pengaturan timesheet ini?", function(result) {
                if (result) {
                    $('#settings').submit();
                }
            });
        });
    </script>
    <script>
        $(document).on("click", "#tombolSubmit", function(e) {
            e.preventDefault();
            bootbox.confirm("Anda yakin ingin menyimpan pengaturan rekap ini?", function(result) {
                if (result) {
                    $('#settings2').submit();
                }
            });
        });
    </script>

    <script src= "http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
    <script>	
    	$("#trigger").click(function() {
    		if($("#trigger").attr('checked') == true){
    			$(".checkall").attr('checked', true);
    		}
    		else{
    			$(".checkall").attr('checked', false);
    		}

		
		});



    </script>
@stop

@section('footer')

@stop