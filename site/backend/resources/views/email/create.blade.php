@extends('app')

@section('title')
	Email
@stop

@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('/js/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('/css/jquery-ui.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/jquery-ui.structure.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/jquery-ui.theme.css') }}">

@stop


@section('content')
<h1>><a href="{!! URL::to('email') !!}"> Pengaturan Email</a> > Ubah</h1>
{!! HTML::ul($errors->all()) !!}
<div class="col-md-6">
<div class="showback">
    <h3 style="margin-top:5px"> Timesheet Reminder </h3><hr/>

    
    <!-- {!! Form::open(['url' => 'email', 'id' => 'settings']) !!} -->
    {!! Form::model($datanotif, ['method'=> 'POST','action'=>['EmailController@storets'], 'id' => 'settings']) !!}



    <div class="form-group">
        Reminder pengisian timesheet akan dikirimkan setiap hari pada jam

        {!! Form::select('jam_notif', [null=>'____','15:00:00'=>'15:00:00','16:00:00'=>'16:00:00','17:00:00'=>'17:00:00','18:00:00'=>'18:00:00',
           '19:00:00'=>'19:00:00', '20:00:00'=>'20:00:00','21:00:00'=>'21:00:00','22:00:00'=>'22:00:00',
           '23:00:00'=>'23:00:00'], $datanotif->jam_notif) !!} WIB.

       </div>

       <div class="form-group form-inline text-center">
        <button type="submit" class="form-control btn btn-primary" id="submitButton"><i class="fa fa-check"></i> Simpan</button>
    </div>
    {!! Form::close() !!}
    </div>
</div>

<div class="col-md-6">
<div class="showback">
    <h3 style="margin-top:5px"> Notifikasi Rekap </h3>
    <hr/>
    <p class="help-block">Hanya user yang dipilih yang akan mendapatkan email rekap timesheet bulanan</p>
    {!! Form::model($settingemail, ['method'=> 'POST','action'=>['EmailController@storerekap'], 'id' => 'settings2']) !!}



    @foreach ($users as $user)
    <br>
    <!-- {!! Form::checkbox('email', $user->email) !!} {{ $user->nama }} ({{ $user->email }})  -->
    {!! Form::checkbox('emails[]', $user->email, (in_array($user->email, $settingemail->lists('email')) ? true : false), ['class'=>'checkall']) !!} 
    <span class="lbl padding-8">{{ $user->nama }} ({{ $user->email }})</span>

    @endforeach


    <br>

    <!-- {!! Form::submit('Save') !!} -->
    <br>
    <div class="form-group form-inline text-center">
        <button type="submit" class="form-control btn btn-primary" id="tombolSubmit"><i class="fa fa-check"></i> Simpan</button>
    </div>

    {!! Form::close() !!}
</div>
</div>



 		
@stop

@section('script')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>

    <script>
    $("#submitButton").click(function(e) {
        e.preventDefault();
        bootbox.confirm("Anda yakin ingin menyimpan pengaturan reminder ini?", function(result){
            if(result) {
                $('#settings').submit();
            }
        });
    });

</script>
<script>
    $("#tombolSubmit").click(function(e) {
        e.preventDefault();
        bootbox.confirm("Anda yakin ingin menyimpan pengaturan notifikasi rekap ini?", function(result){
            if(result) {
                $('#settings2').submit();
            }
        });
    });

</script>


   
    <script>	
    	$("#trigger").click(function() {
    		if($("#trigger").attr('checked') == true)
            {
    			$(".checkall").attr('checked', true);
    		}
    		else
            {
    			$(".checkall").attr('checked', false);
    		}

		
		});
    </script>


    
@stop

@section('footer')

@stop
