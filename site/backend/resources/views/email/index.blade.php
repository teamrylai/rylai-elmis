@extends('app')

@section('title')
Email
@stop

@section('content')
<h1>Pengaturan Email</h1>
<hr>


@if(Session::has('flash_setting'))
<div class=" alert alert-success">{{Session::get('flash_setting')}}</div>
@endif



	<div class="form-group form-inline col-md-12"><a href="{!! URL::to('email/setting') !!}" type="button" class="form-control btn btn-primary">Ubah Pengaturan</a></div>


<div class='col-md-6'>
	<div class="showback">
		<h3 style="margin-top:5px"> Timesheet Reminder </h3><hr/>


		<div class="form-group">
			Reminder pengisian timesheet akan dikirimkan setiap hari pada jam 

			@foreach ($setnotif as $set)
			{{ $set->jam_notif }}

			@endforeach
			WIB.

		</div>

	</div>
</div>

<div class='col-md-6'>
	<div class="showback">
		<h3 style="margin-top:5px"> Notifikasi Rekap </h3>
		<hr/>
		<div class="form-group">User yang akan mendapatkan e-mail rekap timesheet bulanan:</div>

		<div class="form-group">
			<table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th>List Email</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($settings as $setting)
					<tr>
						<td>{{ $setting->email }}</td>
					</tr>	
					@endforeach
				</tbody>
			</table>
		</div>

	</div>
	
</div>



@stop

@section('footer')

@stop