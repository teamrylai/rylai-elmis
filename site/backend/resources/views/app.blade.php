<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

  <link rel="apple-touch-icon" sizes="57x57" href="{{url('img/favicon/apple-icon-57x57.png')}}">
  <link rel="apple-touch-icon" sizes="60x60" href="{{url('img/favicon/apple-icon-60x60.png')}}">
  <link rel="apple-touch-icon" sizes="72x72" href="{{url('img/favicon/apple-icon-72x72.png')}}">
  <link rel="apple-touch-icon" sizes="76x76" href="{{url('img/favicon/apple-icon-76x76.png')}}">
  <link rel="apple-touch-icon" sizes="114x114" href="{{url('img/favicon/apple-icon-114x114.png')}}">
  <link rel="apple-touch-icon" sizes="120x120" href="{{url('img/favicon/apple-icon-120x120.png')}}">
  <link rel="apple-touch-icon" sizes="144x144" href="{{url('img/favicon/apple-icon-144x144.png')}}">
  <link rel="apple-touch-icon" sizes="152x152" href="{{url('img/favicon/apple-icon-152x152.png')}}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{url('img/favicon/apple-icon-180x180.png')}}">
  <link rel="icon" type="image/png" sizes="192x192"  href="{{url('img/favicon/android-icon-192x192.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{url('img/favicon/favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="96x96" href="{{url('img/favicon/favicon-96x96.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{url('img/favicon/favicon-16x16.png')}}">
  <link rel="manifest" href="{{url('img/favicon/manifest.json')}}">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">

    <title>Elmis - @yield('title')</title>

  <!--fonts-->
  <link rel="stylesheet" type="text/css" href="{{ asset('/fonts/font-text/fonts.css')}}">
  <link rel="stylesheet" href="{{ asset('/font-awesome/css/font-awesome.min.css') }}">

  <!--bootstrap css-->
  <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap-select.css')}}">  
  <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap-theme.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap-datetimepicker.css')}}">
  

  <!--component css-->
  <link rel="stylesheet" type="text/css" href="{{ asset('/css/component.css')}}">
  
<script type="text/javascript" src="{{ asset('/js/jquery-1.11.3.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/bootstrap-select.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/bootstrap.file-input.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/moment-with-locales.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/main.js') }}"></script>
<script src="{{ asset('/js/bootstrap-slider.min.js')}}"></script>


  @yield('style')

  @yield('headscript')


</head>

<body>

<header>
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">
            <img class="icon-menu" src="{{url('img/Logo.png')}}">
        </a>
        <a class="navbar-brand title" href="{!! URL::to('home') !!}">
          ELMIS
        </a>
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li>
              <a  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true" href="#">
                {{ \Auth::user()->username }} &nbsp&nbsp<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="{!! URL::to('profile') !!}">Profile</a></li>
                <li class="divider"></li>
                <li><a href="{{url('auth/logout')}}">Logout</a></li>
              </ul>
          </li>
        </ul>
        <ul class="nav navbar-nav navbar-right main-nav">
            <li class="{!! Request::path() == 'home' || Request::path() == '/' ? 'active' : ''; !!}">
              <a href="{!! URL::to('home') !!}">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span> HOME
              </a>
            </li>
            <li class="{!! Request::path() == 'isitimesheet' ? 'active' : ''; !!}">
              <a href="{!! URL::to('isitimesheet/') !!}">
                <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> TIMESHEET
              </a>
            </li>
            <li class="{!! Request::path() == 'rekap' ? 'active' : ''; !!}">
              <a href="{!! URL::to('rekap') !!}">
                <span class="glyphicon glyphicon-book" aria-hidden="true"></span> REKAP
              </a>
            </li>

            @if(\Auth::user()->role_id == '2' || \Auth::user()->role_id == '3')
            <li class="{!! Request::path() == 'formcuti' || Request::path() == 'formlembur' ? 'active' : ''; !!}">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> FORM
              </a>
                <ul class="dropdown-menu" role="menu">
                  <li><a {!! Request::is('formcuti*') ? 'class="active"' : null !!} href="{!! URL::to('formcuti') !!}">Cuti</a></li>
                  <li><a {!! Request::is('formlembur*') ? 'class="active"' : null !!} href="{!! URL::to('formlembur') !!}">Lembur</a></li>                  
                </ul>
            </li>
            @endif

            @if(\Auth::user()->role_id == '1')
            <li class="{!! Request::path() == 'riwayatpekerja' || Request::path() == 'riwayatproyek' ? 'active' : ''; !!}">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                <span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> RIWAYAT
              </a>
                <ul class="dropdown-menu" role="menu">
                  <li><a {!! Request::is('riwayatpekerja*') ? 'class="active"' : null !!} href="{!! URL::to('riwayatpekerja') !!}">Pekerja</a></li>
                  <li><a {!! Request::is('riwayatproyek*') ? 'class="active"' : null !!} href="{!! URL::to('riwayatproyek') !!}">Proyek</a></li>
                </ul>
            </li>
            @endif

            @if(\Auth::user()->role_id == '1')
            <li><a {!! Request::is('statistik*') ? 'class="active"' : null !!} href="{!! URL::to('statistik') !!}"><span class="glyphicon glyphicon-stats" aria-hidden="true"></span> STATISTIK</a></li>            
            @endif

            <li><a {!! Request::is('pesan*') ? 'class="active"' : null !!} href="{!! URL::to('pesan') !!}"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> PESAN</a></li>

            @if(\Auth::user()->role_id == '1')
            <li class="{!! Request::path() == 'user' || Request::path() == 'email' ? 'active' : ''; !!}">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span> PENGATURAN
            </a>
                <ul class="dropdown-menu" role="menu">
                  <!-- <li><a {!! Request::is('user*') ? 'class="active"' : null !!} href="{!! URL::to('user') !!}">User</a></li> -->
                  <li><a {!! Request::is('email*') ? 'class="active"' : null !!} href="{!! URL::to('email') !!}"> Email</a></li>
                </ul>
            </li>
            @endif

          </ul>
      </div>
    </div>
  </nav>
</header>

<div class="container">
  @yield('content')
</div>


</body>
</html>
