@extends('app')

@section('title')
User - {{ $user->nama }}
@stop

@section('content')


<h1>> <a href="{{ action('UserController@index') }}">User</a> > {{ $user->nama }}</h1>
<br><br>
<div class="content-panel">
	<div class="col-md-12">
		<table class="table table-fixed table-striped">
			<tbody>
				<tr>
				<th class="text-right col-md-3 text-right">Nama:</th>
				<td class="col-md-9">{{ $user->nama }}</td>
			</tr>
			<tr>
				<th class="text-right col-md-3 text-right">Username:</th>
				<td class="col-md-9">{{ $user->username }}</td>
			</tr>
				<tr><th class="col-md-3 text-right">NIP:</th><td>{{ $user->nip }}</td></tr>
				<tr><th class="text-right">Email:</th><td>{{ $user->email }}</td></tr>
				<tr><th class="text-right">Nomor Telepon:</th><td>{{ $user->no_telp }}</td></tr>
				<tr><th class="text-right">Role:</th><td>{{ $user->role->role_name }}</td></tr>
				<tr><th class="text-right">Jabatan:</th><td>{{ $user->jabatan }}</td></tr>
				<tr><th class="text-right">Divisi:</th><td>{{ $user->team->team_name }}</td></tr>
			</tbody>
		</table>
	</div>

	
	<div class="form-inline text-center">
		<div class="center-block">
			<a href="{{URL::action('UserController@edit', array($user->nip))}}" type="button" class="form-control btn btn-primary"><span class="glyphicon glyphicon-edit"></span> Edit</a>
			<!-- test -->

			{!! Form::open(array('route' => array('user.destroy', $user->nip), 'method' => 'delete' , 'id' => 'deleteuser', 'class' => 'form-group')) !!}

			<button type="submit" class="form-control btn btn-danger btn-mini" id="submitButton"><span class="glyphicon glyphicon-remove"></span>Delete</button>

			{!! Form::close() !!}
		</div>
	</div>
</diV>

@stop

@section('script')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script>
$(document).on("click", "#submitButton", function(e) {
	e.preventDefault();
	bootbox.confirm("Anda yakin ingin menghapus akun ini?", function(result) {
		if (result) {
			$('#deleteuser').submit();
		}
	});
});
</script>


@stop
