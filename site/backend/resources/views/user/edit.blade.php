@extends('app')

@section('title')
    Edit Akun
@stop


@section('content')
    <h1><a href="{{ action('UserController@index') }}">User</a> > {{ $user->nama }} > Edit</h1>
    <br><br>
    <div class="form-panel">
{!! HTML::ul($errors->all()) !!}
{!! Form::model($user, ['method'=> 'PATCH','action'=>['UserController@update', $user->nip], 'id' => 'edituser']) !!}
        <div class="form-group col-sm-8">
            <table class="table table-fixed">
                <tbody>
                    <tr>
                        <th class="text-right col-sm-2">{!! Form::label('name', 'Name:') !!}</th>
                        <td class="col-sm-10">{!! Form::text('nama', null, array('class' => 'form-control')) !!}</td>
                    </tr>
                    <tr>
                        <th class="text-right">{!! Form::label('email', 'Email:') !!}</th>
                        <td>{!! Form::email('email', null, array('class' => 'form-control')) !!}</td>
                    </tr>
                    <tr>
                        <th class="text-right ">{!! Form::label('username', 'Username:') !!}</th>
                        <td >{!! Form::text('username', null, array('class' => 'form-control')) !!}</td>
                    </tr>
                    <tr>
                        <th class="text-right">{!! Form::label('no_telp', 'No telp:') !!}</th>
                        <td>{!! Form::text('no_telp', null, array('class' => 'form-control')) !!}</td>
                    </tr>
                    <tr>
                        <th class="text-right">{!! Form::label('role_id', 'Role id:') !!}</th>
                        <td>{!! Form::select('role_id', ['1'=>'Admin', '2'=>'Team Leader', '3'=>'User'], $user->role_id) !!}</td>
                    </tr>
                    <tr>
                        <th class="text-right">{!! Form::label('team_id', 'Team id:') !!}</th>
                        <td>{!! Form::select('team_id', ['1'=>'Business', '2'=>'Tech', '3'=>'UX'], $user->team_id) !!}</td>
                    </tr>
                    <tr>
                        <th class="text-right"> {!! Form::label('jabatan', 'Jabatan:') !!}</th>
                        <td>{!! Form::text('jabatan', null, array('class' => 'form-control')) !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="form-group text-center col-sm-8">
            <button type="submit" class="btn btn-primary" id="submitButton"><i class="fa fa-check"></i> Edit user</button>
        </div>
    </div>
    



    {!! Form::close() !!}

    </div>
@stop

@section('script')
    <script src="{{ asset('/js/bootbox.min.js') }}"></script>
    <script>
        $(document).on("click", "#submitButton", function(e) {
            e.preventDefault();
            bootbox.confirm("Anda yakin ingin mengubah akun user ini?", function(result) {
                if (result) {
                    $('#edituser').submit();
                }
            });
        });
    </script>
@stop

@section('footer')