@extends('app')

@section('title')
	User
@stop

@section('content')
	<h1>User </h1>
	<hr>
	

	@if(Session::has('flash_message1'))
	<div class="alert btn-success">{{Session::get('flash_message1')}}</div>
	@endif
	
	@if(Session::has('flash_message2'))
	<div class="alert btn-success">{{Session::get('flash_message2')}}</div>
	@endif


	<div class="form-panel">

	@if(Session::has('deletemessage'))
	<div class="alert btn-success">{{Session::get('deletemessage')}}</div>
	@endif


	<div class="form-group form-inline"><a href="{!! URL::to('user/create') !!}" type="button" class="form-control btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Buat User</a></div>
	<table class="table table-hover">
		<thead>
		<tr>
			<th>NIP</th><th>Nama</th><th>Role</th>
		</tr>
		</thead>
		<tbody>
			
		@foreach ($users as $user)
			<tr style="font-size:12pt;">
				<td>{{ $user->nip }}</td>
				<td><a href="{{ action('UserController@show', $user->nip) }}">{{ $user->nama }}</a></td>
				<td>{{ $user->role->role_name }}</td>
			</tr>	
		@endforeach
		</tbody>
	</table>
	<center>{!! $users->render() !!}</center>

	</div>
@stop

@section('script')
<script>
jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.document.location = $(this).data("href");
    });
});
</script>

@stop