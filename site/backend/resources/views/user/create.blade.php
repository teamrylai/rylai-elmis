@extends('app')

@section('title')
User - Buat Akun
@stop


@section('content')
<h1>> <a href="{{ action('UserController@index') }}">User</a> > Buat Akun</h1>
<div class="form-panel">
    {!! HTML::ul($errors->all()) !!}
    {!! Form::open(['url' => 'user', 'id' => 'createuser']) !!}

    <div class="col-md-6 form-group">
        <h4>> Informasi Pribadi</h4>
        <hr>
        <div class="form-group form-inline">
            <div class="form-group">
                {!! Form::label('name', 'Nama') !!}<br>
                {!! Form::text('nama', Input::old('name'), array('class' => 'form-control')) !!}
                
            </div>
            <div class="form-group">
                {!! Form::label('no_telp', 'Nomor Telepon') !!}<br>
                {!! Form::text('no_telp', Input::old('no_telp'), array('class' => 'form-control')) !!}
            </div>
            

        </div>

        <div class="form-inline form-group">    
            <div class="form-group">
                {!! Form::label('id', 'Role ') !!}
                <br>
                {!! Form::select('id', $roles, null, array('class' => 'form-control selectpicker')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('team_id', 'Team ') !!}
                <br>
                {!! Form::select('team_id', $teams, null, array('class' => 'form-control selectpicker')) !!}
            </div>



            <div class="form-group">
                {!! Form::label('jabatan', 'Jabatan') !!}
                <br>
                {!! Form::text('jabatan', Input::old('jabatan'), array('class' => 'form-control')) !!}
            </div>
        </div>
        
    </div>

    <div class="col-md-6 form-group">
        <h4>>   Informasi Akun</h4>
        <hr>
        <div class="form-inline form-group">
            <div class="form-group">
                {!! Form::label('nip', 'NIP') !!}<br>
                {!! Form::text('nip', Input::old('nip'), array('class' => 'form-control', 'maxlength'=>'10')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('username', 'Username') !!}<br>
                {!! Form::text('username', Input::old('username'), array('class' => 'form-control')) !!}
            </div>



        </div>    
        <div class="form-group form-inline">
            {!! Form::label('email', 'Email') !!}<br>
            {!! Form::email('email', Input::old('email'), array('class' => 'form-control')) !!}
        </div> 
        <div class="form-inline form-group">

            <div class="form-group">
                {!! Form::label('password', 'Password') !!}
                <br>
                {!! Form::password('password', array('class' => 'form-control')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('confirm_password', 'Konfirmasi Password') !!}
                <br>
                {!! Form::password('confirm_password', array('class' => 'form-control')) !!}
            </div>
        </div>
    </div>
    <div class="form-group form-inline text-center">
            <button type="submit" class="form-control btn btn-primary" id="submitButton"><i class="fa fa-check"></i> Simpan</button>
        </div>
    
       
    

    {!! Form::close() !!}
</div>


@stop

@section('script')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script>
$(document).on("click", "#submitButton", function(e) {
    e.preventDefault();
    bootbox.confirm("Anda yakin ingin membuat akun user ini?", function(result) {
        if (result) {
            $('#createuser').submit();
        }
    });
});
</script>
@stop


@section('footer')



