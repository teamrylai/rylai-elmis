@extends('app')

@section('title')
Kirim Pesan
@stop


@section('content')
	<h1>Balas Pesan</h1>
    <br><br>
    <div class="form-horizontal">
    {{ HTML::ul($errors->all()) }}
	{!! Form::open(['route' => 'pesan.store']) !!}  
   

    {!! Form::model($message, ['method'=> 'POST','action'=>['PesanController@store'], 'id' => 'msg2']) !!}
    
            {{--dari admin ke nip penerima--}}



            <div class="form-group">
                <div class="control-label col-sm-2">{!! Form::label('penerima', 'NIP Penerima:') !!}</div>
                    <div class="col-sm-8">
                        {!! Form::text('arrpenerima[]', $message->id_pengirim ,array('class' => 'form-control', 'placeholder' => 'nip user','readonly'=>'readonly')) !!}   
                    </div>
            </div>
            <div class="form-group">
                <div class="control-label col-sm-2">
                    {!! Form::label('subject', 'Subject:') !!}
                </div>
                <div class="col-sm-8">
                {!! Form::text('subjek', null, array('class' => 'form-control','readonly'=>'readonly')) !!}
                </div>
            </div>

             <div class="form-group">
                <div class="control-label col-sm-2">
                    {!! Form::label('message', 'Message') !!}
                </div>
                <div class="col-sm-8">
                {!! Form::textarea('isi', null, array('class' => 'form-control','placeholder' => 'Message','required'=>'required')) !!}
                </div>
             </div>
		<div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
			{!! Form::submit('Kirim', ['class'=>'btn btn-primary']) !!}
            {{--{!! Form::submit('Create', array('class'=>'btn btn-primary' )) !!}--}}
            </div>
		</div>
			
	{!! Form::close() !!}
</div>
@stop

@section('footer')