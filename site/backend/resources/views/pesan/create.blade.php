@extends('app')

@section('title')
Kirim Pesan
@stop


@section('content')
<h1>Kirim Pesan</h1>
<div class="form-panel">
    {{ HTML::ul($errors->all()) }}
    <!-- {!! Form::open(['route' => 'pesan.store']) !!} -->
    {{--dari admin ke nip penerima--}}

    <div class="form-group col-md-3">
        <p class="help-block">Kirim ke: </p>
        {!! Form::open(['url' => 'pesan', 'id' => 'idmsgform']) !!}

        
        
              {!! Form::checkbox('checkall',null, false, ['id'=>'trigger1', 'class'=>'checkbox']) !!}
              <span class="lbl padding-8">Semua</span>
        
        
            
        @foreach ($users as $user)
        
        <br>
        
            {!! Form::checkbox('arrpenerima[]', $user->nip, false,['class'=>'checkbox']) !!} 
            <span class="lbl padding-8">{{ $user->nama }}</span>            
        
        
        @endforeach
    </div>

    <div class="form-group col-md-9">
    <div class="form-group">

        {!! Form::label('subject', 'Subject') !!}
        {!! Form::text('subjek', null, array('class' => 'form-control','required'=>'required')) !!}

    </div>


    <div class="form-group">

        {!! Form::label('message', 'Pesan') !!}
        {!! Form::textarea('isi', null, array('class' => 'form-control','required'=>'required')) !!}

    </div>


    <div class="form-group form-inline text-center">
       {!! Form::submit('Kirim', ['class'=>'btn btn-primary form-control']) !!}
   </div>
   </div>
</div>



{!! Form::close() !!}
</div>
@stop

@section('script')
<script src= "http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
<script>    
$("#trigger1").click(function() {
    if($("#trigger1").attr('checked') == true){
        $(".checkbox").attr('checked', true);
    }
    else{
        $(".checkbox").attr('checked', false);
    }


});



</script>

@stop


@section('footer')