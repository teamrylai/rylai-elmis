@extends('app')

@section('title')
    Daftar Pesan
@stop

@section('content')
    
    

    <h1>Kotak Masuk </h1>
    <hr>
    @if(Session::has('flash_message'))
    <div class="alert btn-success">{{Session::get('flash_message')}}</div>
    @endif
    <div class="form-panel">
        <div class="form-group form-inline">
        <a href="{{URL::action('PesanController@create')}}" type="button" class="form-control btn btn-primary"><span class="glyphicon glyphicon-envelope"></span> Kirim Pesan</a>
    </div>
         
    <table class="table table-hover">
        <thead>
        <tr>
            <th>ID</th><th>Pengirim</th><th>Subjek</th>
        </tr>
        </thead>
        <tbody>
            {{-- hahaha --}}

        @foreach ($messages as $pesan)
            <tr style="font-size:14pt;">
                <td><a href="{{ action('PesanController@show', $pesan->id) }}">{{$pesan->id}}</a></td>
                <td><a href="{{ action('PesanController@show', $pesan->id) }}">{{$pesan->id_pengirim}}</a></td>
                <td><a href="{{ action('PesanController@show', $pesan->id) }}">{{$pesan->subjek}}</a></td>
            </tr>   
        @endforeach
        </tbody>
    </table>
    
    {!! $messages->render() !!}
</div>
@stop

@section('script')
<script>
jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.document.location = $(this).data("href");
    });
});

</script>
@stop