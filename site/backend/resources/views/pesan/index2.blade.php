@extends('app')

@section('title')
Daftar Pesan
@stop

@section('content')



<h1>> Pesan  </h1>
@if(Session::has('flash_pesan'))
<div class="alert btn-success">{{Session::get('flash_pesan')}}</div>
@endif
<div class="form-panel">
    <div class="form-group form-inline">
        <a href="{{URL::action('PesanController@create')}}" type="button" class="form-control btn btn-primary"><i class="fa fa-envelope"></i> Kirim Pesan</a>
    </div>
    <div class="form-group">
    <table class="table table-hover">
        <thead>
            <tr>
                <th>ID</th><!-- <th>Pengirim</th> --><th>Subjek</th>
            </tr>
        </thead>
        <tbody>
            

            @foreach ($messages as $pesan)
            <tr style="font-size:14pt;">
                <td><a href="{{ action('PesanController@show', $pesan->id) }}">{{ $pesan->id }}</a></td>
                <!-- <td>{{ $pesan->id_penerima}}</td> -->
                <td><a href="{{ action('PesanController@show', $pesan->id) }}">{{ $pesan->subjek}}</a></td>
                
            </tr>   
            @endforeach
        </tbody>
    </table>
</div>
    
    {!! $messages->render() !!}
</div>
@stop

@section('footer')

@stop