@extends('app')

@section('title')
{{ $message->subjek }}
@stop

@section('content')
	
<h1>> <a href="{{ action('PesanController@index') }}" >Pesan</a> > {{ $message->subjek }}</h1>
 
    <br><br>
    <form class="form-horizontal" role="form">
    <div class="form-group">
      <label class="control-label col-sm-2">Pengirim:</label>
      <div class="col-sm-8">
        <input class="form-control" value="Admin" readonly="">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Isi:</label>
      <div class="col-sm-8">          
        <textarea class="form-control" rows="7" readonly="">{{ $message->isi }}</textarea>
      </div>
    </div>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <a href="{{URL::action('PesanController@balas', array($message->id))}}" type="button" class="btn btn-primary">Balas</a>
      </div>
    </div>
  </form>
@stop

@section('footer')