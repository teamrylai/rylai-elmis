@extends('app')

@section('title')
Kirim Pesan
@stop


@section('content')
	<h1>Kirim Pesan</h1>
    <div class="form-panel">
    {{ HTML::ul($errors->all()) }}
	{!! Form::open(['route' => 'pesan.store']) !!}  {{--{!! Form::model($user, ['method'=> 'PATCH','action'=>['UserController@update', $user->nip]]) !!}--}}
    {{--{!! Form::model($pesan,['route' => 'pesan.store']) !!} --}}


            <div class="form-group">
                {!! Form::label('subject', 'Subject') !!}
                {!! Form::text('subjek', null, array('class' => 'form-control','required'=>'required')) !!}
            </div>

             <div class="form-group">

                {!! Form::label('message', 'Pesan') !!}
                {!! Form::textarea('isi', null, array('class' => 'form-control','required'=>'required')) !!}

             </div>

		<div class="form-group form-inline text-center">
			{!! Form::submit('Kirim', ['class'=>'btn btn-primary form-control']) !!}
            {{--{!! Form::submit('Create', array('class'=>'btn btn-primary' )) !!}--}}
		</div>
		
			
	{!! Form::close() !!}
</div>
@stop

@section('footer')