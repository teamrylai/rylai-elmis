<<<<<<< HEAD
<?php

=======
>>>>>>> 7ba600b4a690836f48308642a7dd881ae4a3c69c
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Saka UI Kit</title>
  <meta name="description" content="UI Kit.">
  <meta name="author" content="Faizal Rahman">

  <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
  <link rel="manifest" href="img/favicon/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">

  <meta name="viewport" content="width=device-width,initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" />

  <!-- Fonts -->
  <link rel="stylesheet" href="fonts/font-text/fonts.css">

  <!-- CSS Bootstrap -->
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap-select.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap-slider.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap-datetimepicker.css">

  <!-- CSS Component -->
  <link rel="stylesheet" href="css/component.css">

</head>
<body>

  // <header>
  //   <nav class="navbar navbar-default navbar-fixed-top">
  //     <div class="container-fluid">
  //       <!-- Brand and toggle get grouped for better mobile display -->
  //       <div class="navbar-header">
  //         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
  //           <span class="sr-only">Toggle navigation</span>
  //           <span class="icon-bar"></span>
  //           <span class="icon-bar"></span>
  //           <span class="icon-bar"></span>
  //         </button>
  //         <a class="navbar-brand" href="#">
  //           <img class="icon-menu" src="img/Logo - Saka.png">
  //         </a>
  //       </div>
  //     </div><!-- /.container-fluid -->
  //   </nav>
  // </header>

  <section id="content" class="login">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <h1>Platform Title</h1>
          <form>

            <div class="form-inline">

              <div class="form-group">
                <label class="sr-only" for="exampleInputEmail3">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Enter email">
              </div>

              <div class="form-group">
                <label class="sr-only" for="exampleInputPassword3">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword3" placeholder="Password">
              </div>

              <button type="submit" class="btn btn-secondary">Login</button>

            </div>

            <div class="form-group">
              Haven't register yet? <a href="">Register here</a>
            </div>

            <div class="vertical-separator"></div>

            <div class="form-group">
              <button type="submit" class="btn btn-danger">Login via Google Account</button>
            </div>

          </form>
        </div>
      </div>
    </div>
  </section>

  <footer>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6 copyright">
          All Right Reserved © Saka Digital, PT Saka Digital Arsana
        </div>
        <div class="col-md-6">
          <ul class="menu list-inline">
            <li><a href="">About</a></li>
            <li><a href="">Terms &amp; Conditions</a></li>
            <li><a href="">Privacy Policy</a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.js"></script>
  <script type="text/javascript" src="js/bootstrap-select.js"></script>
  <script type="text/javascript" src="js/bootstrap-slider.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.file-input.js"></script>
  <script type="text/javascript" src="js/moment-with-locales.js"></script>
  <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
  <script type="text/javascript" src="js/main.js"></script>
</body>
</html>