@extends('app')

@section('title')
Statistik
@stop
@section('content')
<h1> Statistik</h1>
<hr>

<div class="form-panel" >
		<div class="form-group form-inline">
		{!! Form::select('arrproject', $arrproject, $default, ['class'=>'form-control selectpicker', 'onchange'=>'document.location=this.options[this.selectedIndex].value;']) !!}
	</form>
</div>

@foreach ($listproject as $key => $value)
  <div class="row">
  	<div class="col-sm-8">
  		<div id="{{ $key }}"></div>
    	<?php echo $value->render('BarChart', 'Hour Spent', $key); ?>
  	</div>
  </div>
@endforeach
@stop

@section('script')

@stop