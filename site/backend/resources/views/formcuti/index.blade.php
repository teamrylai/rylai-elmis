@extends('app')

@section('title')
	Daftar Form Cuti
@stop

@section('content')
	<h1> User </h1>

	@if(Session::has('flash_message1'))
	<div class=" alert alert-success">{{Session::get('flash_message1')}}</div>
	@endif
	
	@if(Session::has('flash_message2'))
	<div class=" alert alert-success">{{Session::get('flash_message2')}}</div>
	@endif


	<div class="content-panel">

	@if(Session::has('deletemessage'))
	<div class=" alert alert-success">{{Session::get('deletemessage')}}</div>
	@endif

@stop

@section('footer')

@stop