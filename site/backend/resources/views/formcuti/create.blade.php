@extends('app')

@section('title')
Isi Form Cuti
@stop

@section('style')


@stop

@section('content')
<h1>> <a href="{{ action('FormCutiController@index') }}">Form Cuti</a> > Isi Form</h1>

<div class="form-panel col-md-8">
	{!! Form::open(['url' => 'formcuti/create', 'id' => 'formcuti']) !!}
	<!-- Jenis Cuti Form Input -->

	<br><br>
	<div class="form-group">
		{!! Form::select('jenis_cuti', App\JenisCuti::lists('jenis_cuti'), null, ['class'=>'form-control selectpicker']) !!}
	</div>
	<!-- Waktu Form Input -->
	<!-- Waktu Form Input -->
			<div class="form-group">
              <div class='input-group date'>
                <span class="input-group-addon">
                    <button class="btn btn-success" type="button">
                      <span class="glyphicon glyphicon-calendar"></span> TANGGAL MULAI
                    </button>
                </span>
                <input type='text' name="tanggal_mulai" class="form-control" placeholder="Tanggal Cuti" />
              </div>
          	</div>
          	
          	<div class="form-group">
              <div class='input-group date'>
                <span class="input-group-addon">
                    <button class="btn btn-success" type="button">
                      <span class="glyphicon glyphicon-calendar"></span> TANGGAL SELESAI
                    </button>
                </span>
                <input type='text' name="tanggal_selesai" class="form-control" placeholder="Tanggal Selesai"/>
              </div>
          	</div>
			
			
		
	    <p class="help-block">*Kosongkan tanggal selesai jika cuti satu hari</p>

		
	
	
	<div class="form-group {{ $errors->has('alasan') ? 'has-error' : '' }}">
		{!! Form::textarea('alasan', null, ['class'=>'form-control', 'placeholder' => 'Keterangan']) !!}
		{!! $errors->first('alasan', '<span class="help-block">:message</span>') !!}
	</div>

	<div class="form-group form-inline text-center">
	<button type="submit" class="form-control btn btn-primary" id="submitButton"><i class="fa fa-check"></i> Simpan</button>
</div>
	
</div>
<!-- Add Article Form Input hahaha-->


{!! Form::close() !!}


{{-- 	@if ($errors->any())
		<ul class="alert alert-danger">
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
		@endif --}}

		@stop


		@section('script')
		<script src="{{ asset('/js/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
		


		<script src="{{ asset('/js/bootbox.min.js') }}"></script>
		<script>
		$(document).on("click", "#submitButton", function(e) {
			e.preventDefault();
			bootbox.confirm("Anda yakin ingin menyimpan form ini?", function(result) {
				if (result) {
					$('#formcuti').submit();
				}
			});
		});
		</script>
		@stop
