@extends('app')

@section('title')
Form Cuti
@stop


@section('content')
<h1>> Form Cuti</h1>
{{--  Hahahhahahahahah --}}
<div class="form-panel" >
    <div class="text-center">
        {{-- Menampilkan notifikasi sukses --}}
        @if (Session::get('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
        @endif
    </div>
    <div class="form-inline form-group">
        <a href="{{ action('FormCutiController@create') }}" type="button" class="form-control btn btn-primary"><i class="fa fa-file"></i> Buat Form Cuti</a></td>
    </div>
    
    <table class="table table-hover">
        <thead>
         <tr>
          <th>Jenis Cuti</th><th>Mulai Cuti</th><th>Selesai Cuti</th><th></th>
      </tr>
  </thead>
  <tbody>
        @foreach ($forms as $form)
        @if($form['jenis_form'] == 'cuti')
        <tr>

            <td>{{ $form->formcuti->jeniscuti['jenis_cuti'] }}</td>
            <td>{{ $form->formcuti->tgl_mulai }}</td>
            <td>{{ $form->formcuti->tgl_selesai }}</td>
            <td><a target = '_blank' href="{{ URL::to('formcuti/download') }}/{!! $form->kode !!}" type="button" class="btn btn-primary btn-xs">Download</a></td>
        </tr>
        @endif
        @endforeach
    </tbody>
</table>

</div>


@stop