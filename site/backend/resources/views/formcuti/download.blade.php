<html>
	<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

  <link rel="apple-touch-icon" sizes="57x57" href="{{url('img/favicon/apple-icon-57x57.png')}}">
  <link rel="apple-touch-icon" sizes="60x60" href="{{url('img/favicon/apple-icon-60x60.png')}}">
  <link rel="apple-touch-icon" sizes="72x72" href="{{url('img/favicon/apple-icon-72x72.png')}}">
  <link rel="apple-touch-icon" sizes="76x76" href="{{url('img/favicon/apple-icon-76x76.png')}}">
  <link rel="apple-touch-icon" sizes="114x114" href="{{url('img/favicon/apple-icon-114x114.png')}}">
  <link rel="apple-touch-icon" sizes="120x120" href="{{url('img/favicon/apple-icon-120x120.png')}}">
  <link rel="apple-touch-icon" sizes="144x144" href="{{url('img/favicon/apple-icon-144x144.png')}}">
  <link rel="apple-touch-icon" sizes="152x152" href="{{url('img/favicon/apple-icon-152x152.png')}}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{url('img/favicon/apple-icon-180x180.png')}}">
  <link rel="icon" type="image/png" sizes="192x192"  href="{{url('img/favicon/android-icon-192x192.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{url('img/favicon/favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="96x96" href="{{url('img/favicon/favicon-96x96.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{url('img/favicon/favicon-16x16.png')}}">
  <link rel="manifest" href="{{url('img/favicon/manifest.json')}}">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">

    <title>Elmis - {{ $form->formcuti->jeniscuti['jenis_cuti'] }}</title>
</head>

<br><center><u><h2>LEAVE FORM (FORMULIR PERMOHONAN CUTI)</h2></u></center></br>



<body>
<table align="center">
		
		 <tbody align="left">

			<tr>
				<th><i>Division (Divisi)</i></th> <th>:  </th>
				<th><b>{{ \Auth::user()->team['team_name'] }}</b></th>
			</tr>

			<tr>
				<th><i>Section (Seksi)</i></th> <th>:  </th>
				<th><b>{{ \Auth::user()->jabatan }}</b></th>
			</tr>

		 	<tr>
				<th><i>Employee (Nama Karyawan)</i></th> <th>:  </th>
				<th><b>{{ \Auth::user()->nama }}</b></th>
			</tr>

			<tr>
				<th><i>Employee Number (NIP)</i></th> <th>:  </th>
				<th><b>{{ \Auth::user()->nip }}</b></th>
			</tr>

			<tr>
				<th><i>Type (Jenis Cuti)</i></th> <th>:  </th>
				<th><b>{{ $form->formcuti->jeniscuti['jenis_cuti'] }}</b></th>
			</tr>

			<tr>
				<th><i>Begin (Tanggal Mulai Cuti)</i></th> <th>:  </th>
				<th><b>{{ $form->formcuti->tgl_mulai }}</b></th>

			</tr>

			<tr>
				<th><i>Finish (Tanggal Selesai Cuti)</i></th> <th>:  </th>
				<th><b>{{ $form->formcuti->tgl_selesai }}</b></th>
			</tr>

			<tr>
				<th><i>Reason (Alasan)</i></th> <th>:  </th>
				<th><b>{{ $form->formcuti->alasan }}</b></th>
			</tr>
		
		</tbody>
		</table>
</body>

<footer align="center">
		<br>Remarks (Catatan/Komentar/Tambahan)</br>
		<br></br>
		<br></br>
		<br></br>
		<br></br>
		<br></br>

		<table align="center">
		<tbody align="center">
		<tr>
			<td>Approved by Employee</td>
			<td>Approved by Supervisor</td>
		</tr>
		<tr>
			<td><img src="{!! asset(\Auth::user()->path) !!}"></td>
			
			
				@if(isset($teamleader))
				<td><img src="{{ asset($teamleader->path) }}"></td>
				@else
				<td></td>
				@endif			
			
			

		</tr>
		<tr>
			<td><b>{{ \Auth::user()->nama }}</b></td>
			
				@if(isset($teamleader))
				<td><b>{{ $teamleader->nama }}</b></td>
				@else
				<td></td>
				@endif
			 
		</tr>

		</tbody>
		</table>
		
</footer>
</html>