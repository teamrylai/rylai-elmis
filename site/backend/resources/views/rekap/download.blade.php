<html>
<head>
	{{-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> --}}
	<style>

		table {
		    border-collapse: collapse;
		}
		table, td, th {
		    border: 1px solid #000000;
		}
		.header{
			color: #ffffff;
			background: #292828;
			border: 1px solid #000000;
			text-align: center;
		}
		.sa{
			text-align: center; background:#ececec;
		}
		.si{
			text-align: center;
		}
		.bulan{
			text-align: center;
		}
		.a{
			text-align:center; width:3.4; 
		}
		.t{
			text-align:center; background:#dbdada; width:3.4;
		}
	</style>
</head>
<body>

 @if(\Auth::user()->role_id == 1)
	<table id="tabel">
		 
		
		<thead>
			<tr><th class="bulan" colspan={{ (2*count($tanggal))+1 }}>{{ $cal }} 2015</th></tr>
			<tr>
				<th rowspan=2 class="header">Nama Pekerja</th>
				@foreach ($tanggal as $date)
					<th colspan=2 class="header">{{ \Carbon\Carbon::parse($date)->format('d') }}</th>
				@endforeach
			</tr>
			<tr>
				<th></th>
				@foreach ($tanggal as $date)
					<th class="a">A</th>
					<th class="t">T</th>
				@endforeach
			</tr>
		</thead>
		 <tbody>
			@for ($i = 0; $i < count($users); $i++)
				<tr>
					<td>{{ $users[$i]['nama'] }}</td>

					@for ($j = 0; $j < count($tanggal); $j++)
						@if ($hadir = NULL) @endif  
						@if ($timesheet = \App\Timesheet::where('nip_pekerja', '=', $users[$i]['nip'])->where('tanggal', '<=', \Carbon\Carbon::now())->get()->sortBy('tanggal')) @endif 
						
						@foreach($timesheet as $ts)
							@if($ts['tanggal'] == $tanggal[$j])
								@if ($hadir = $ts) @endif 
							@endif
						@endforeach
					
						@if($hadir != NULL)
							 
							@if($hadir['status_absensi'] == 'H')
								<td class="sa"><span class="label label-success label-mini">{{ $hadir['status_absensi'] }}</td>
							@elseif($hadir['status_absensi'] == 'C')
								<td class="sa"><span class="label label-default label-mini">{{ $hadir['status_absensi'] }}</td>
							@elseif($hadir['status_absensi'] == 'W')
								<td class="sa"><span class="label label-primary label-mini">{{ $hadir['status_absensi'] }}</td>
							@elseif($hadir['status_absensi'] == 'D')
								<td class="sa"><span class="label label-info label-mini">{{ $hadir['status_absensi'] }}</td>
							@elseif($hadir['status_absensi'] == 'S')
								<td class="sa"><span class="label label-default label-mini">{{ $hadir['status_absensi'] }}</td>
							@else
								<td class="sa">{{ $hadir['status_absensi'] }}</td>
							@endif
							
							@if($hadir['status_isian'] == 'OK' && $hadir['created_at']->format('Y-m-d') > $hadir['tanggal'])
								<td class="si"><span class="label label-warning">{{ $hadir['status_isian'] }}</td>
							@elseif($hadir['status_isian'] == 'OK' && $hadir['created_at']->format('Y-m-d') == $hadir['tanggal'])
								<td class="si"><span class="label label-success">{{ $hadir['status_isian'] }}</td>
							@elseif($hadir['status_isian'] == 'C')
								<td class="si"><span class="label label-default">{{ $hadir['status_isian'] }}</td>
							@elseif($hadir['status_isian'] == 'S')
								<td class="si"><span class="label label-default">{{ $hadir['status_isian'] }}</td>
							@else
								<td class="si">{{ $hadir['status_isian'] }}</td>
							@endif
							
						@else
							<td class="sa"><span class="label label-danger label-mini">X </td>
							<td class="si"><span class="label label-danger label-mini">X </td>
						@endif
					@endfor
			</tr>
			@endfor
		</tbody>
			 
	</table><br>

	
@endif
</body>
</html>
