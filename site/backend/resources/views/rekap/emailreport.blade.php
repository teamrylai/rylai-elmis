<table id="example" border="1">
	
	<thead>
		<tr><th>{{$cal}} 2015</th></tr>
		<tr>
			<th rowspan=2>Nama Pekerja</th>
			@foreach ($tanggal as $date)
				<th colspan=2 style="text-align:center">{{ \Carbon\Carbon::parse($date)->format('d') }}</th>
			@endforeach
		</tr>
		<tr>
			<th></th>
			@foreach ($tanggal as $date)
				<th style="text-align:center">A</th>
				<th style="text-align:center">T</th>
			@endforeach
		</tr>
	</thead>
	 <tbody>
		@for ($i = 0; $i < count($users); $i++)
			<tr>
				<td>{{ $users[$i]['nama'] }}</td>

				@for ($j = 0; $j < count($tanggal); $j++)
					@if ($hadir = NULL) @endif  
					@if ($timesheet = \App\Timesheet::where('nip_pekerja', '=', $users[$i]['nip'])->where('tanggal', '<=', \Carbon\Carbon::now())->get()->sortBy('tanggal')) @endif 
					
					@foreach($timesheet as $ts)
						@if($ts['tanggal'] == $tanggal[$j])
							@if ($hadir = $ts) @endif 
						@endif
					@endforeach
				
					@if($hadir != NULL)
						 
						@if($hadir['status_absensi'] == 'H')
							<td><span class="label label-success label-mini">{{ $hadir['status_absensi'] }}</td>
						@elseif($hadir['status_absensi'] == 'C')
							<td><span class="label label-default label-mini">{{ $hadir['status_absensi'] }}</td>
						@elseif($hadir['status_absensi'] == 'W')
							<td><span class="label label-primary label-mini">{{ $hadir['status_absensi'] }}</td>
						@elseif($hadir['status_absensi'] == 'D')
							<td><span class="label label-info label-mini">{{ $hadir['status_absensi'] }}</td>
						@elseif($hadir['status_absensi'] == 'S')
							<td><span class="label label-default label-mini">{{ $hadir['status_absensi'] }}</td>
						@else
							<td>{{ $hadir['status_absensi'] }}</td>
						@endif
						
						@if($hadir['status_isian'] == 'OK' && $hadir['created_at']->format('Y-m-d') > $hadir['tanggal'])
							<td><span class="label label-warning">{{ $hadir['status_isian'] }}</td>
						@elseif($hadir['status_isian'] == 'OK' && $hadir['created_at']->format('Y-m-d') == $hadir['tanggal'])
							<td><span class="label label-success">{{ $hadir['status_isian'] }}</td>
						@elseif($hadir['status_isian'] == 'C')
							<td><span class="label label-default">{{ $hadir['status_isian'] }}</td>
						@elseif($hadir['status_isian'] == 'S')
							<td><span class="label label-default">{{ $hadir['status_isian'] }}</td>
						@else
							<td style="font-weight:bold; color:red;">{{ $hadir['status_isian'] }}</td>
						@endif
						
					@else
						<td><span class="label label-danger label-mini"></td>
						<td><span class="label label-danger label-mini"></td>
					@endif
				@endfor
		</tr>
		@endfor
	</tbody>
		 
</table>