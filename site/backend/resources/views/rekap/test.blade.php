@extends('app')

@section('title')
Rekap
@stop


@section('content')
		
	<h1> Rekap </h1>
	<hr>
	<div class="form-panel" >
		
		<div class="form-group form-inline">
			<div class="form-group">
				{!! Form::select('bulan', $bulans, $x, ['class'=>'form-control selectpicker', 'onchange'=>'document.location=this.options[this.selectedIndex].value;']) !!}						
			</div>
			@if(\Auth::user()->role_id == '1')
			<div class="form-group form-inline">
				<a href="{{URL::to('rekap/download', $x)}}" type="button" class="form-control btn btn-primary"><span class="glyphicon glyphicon-download-alt"></span> Download Rekap</a>
			</div>
			@endif
		</div>
	</div>

	
	<div class="form-group">	
		<div class="table-responsive" >
			<div class="table-outer">
				<div class="table-rekap" >

				{{-- TAMPILAN TABLE ADMIN--}}
				@if(\Auth::user()->role_id == 1)
				<table id="example" class="table table-fixed table-striped">
					<thead>
						<tr>
							<th rowspan=2>Nama Pekerja</th>
							@foreach($tanggal as $tgl)
								<th colspan="2" style="text-align:center">{{ \Carbon\Carbon::parse($tgl)->format('d') }}</th>
							@endforeach
						</tr>
						<tr>						
							@foreach($tanggal as $tgl)	
								<th style="text-align:center">A</th>
								<th style="text-align:center">T</th>
							@endforeach
						</tr>
					</thead>
					<tbody>						
						@foreach($users as $user)
						<tr>
							<td style="min-width:175px">{{ $user['nama'] }}</td>

							@foreach($tanggal as $tgl)
								@if ($hadir = NULL) 
									<td></td>
								@endif

								@if ($timesheet = \App\Timesheet::where('nip_pekerja', '=', $user['nip'])->where('tanggal', '<=', \Carbon\Carbon::now())->get()->sortBy('tanggal')) 
								@endif 

								@foreach($timesheet as $ts)
									@if($ts['tanggal'] == $tgl)
									@if ($hadir = $ts) 
									@endif 
									@endif
								@endforeach

								@if($hadir != NULL)
								@if($hadir['status_absensi'] == 'H')
								<td align="center"><span class="label label-success label-mini">{{ $hadir['status_absensi'] }}</td>
								@elseif($hadir['status_absensi'] == 'C')
								<td><span class="label label-default label-mini">{{ $hadir['status_absensi'] }}</td>
								@elseif($hadir['status_absensi'] == 'W')
								<td><span class="label label-primary label-mini">{{ $hadir['status_absensi'] }}</td>
								@elseif($hadir['status_absensi'] == 'D')
								<td><span class="label label-info label-mini">{{ $hadir['status_absensi'] }}</td>
								@elseif($hadir['status_absensi'] == 'S')
								<td><span class="label label-default label-mini">{{ $hadir['status_absensi'] }}</td>
								@else
								<td>{{ $hadir['status_absensi'] }}</td>

								@endif

								@if($hadir['status_isian'] == 'OK' && $hadir['created_at']->format('Y-m-d') > $hadir['tanggal'])
								<td style="background-color:#f0ad4e; min-width:27px;" align="center	"><span class="label label-warning label-mini">{{ $hadir['status_isian'] }}</td>
								@elseif($hadir['status_isian'] == 'OK' && $hadir['created_at']->format('Y-m-d') == $hadir['tanggal'])
								<td style="background-color:#5cb85c; min-width:27px" align="center"><span class="label label-success label-mini">{{ $hadir['status_isian'] }}</td>
								@elseif($hadir['status_isian'] == 'C')
								<td style="background-color:#777; min-width:27px" align="center"><span class="label label-default">{{ $hadir['status_isian'] }}</td>
								@elseif($hadir['status_isian'] == 'S')
								<td style="background-color:#777; min-width:27px" align="center"><span class="label label-default">{{ $hadir['status_isian'] }}</td>
								@else
								<td style="font-weight:bold; color:red;">{{ $hadir['status_isian'] }}</td>
								@endif

								@else
								<td style="min-width:27px"></td>
								<td style="min-width:27px"></td>
								@endif


							@endforeach

						</tr>
						@endforeach
					</tbody>
				</table>
				@endif

				{{-- TAMPILAN TABLE TEAM LEADER --}}
				@if(\Auth::user()->role_id == 2)
				<table id="example" class="table table-fixed table-striped table-bordered table-hover table-condensed">
					<thead>
						<tr>
							<th rowspan=2>Nama Pekerja</th>
							@foreach ($tanggal as $date)
							<th colspan=2 style="text-align:center">{{ \Carbon\Carbon::parse($date)->format('d') }}</th>
							@endforeach
						</tr>
						<tr>
							@foreach ($tanggal as $date)
							<th style="text-align:center">A</th>
							<th style="text-align:center">T</th>
							@endforeach
						</tr>
					</thead>
					<tbody>
						@foreach ($users as $user)
						<tr>
							<td style="min-width:175px">{{ $user['nama'] }}</td>

							@foreach ($tanggal as $date)

							@if ($hadir = NULL) @endif  
							@if ($timesheet = \App\Timesheet::where('nip_pekerja', '=', $user['nip'])->where('tanggal', '<=', \Carbon\Carbon::now())->get()->sortBy('tanggal')) @endif 

							@foreach($timesheet as $ts)
							@if($ts['tanggal'] == $date)
							@if ($hadir = $ts) @endif 
							@endif
							@endforeach

							@if($hadir != NULL)

							@if($hadir['status_absensi'] == 'H')
							<td align="center"><span class="label label-success label-mini">{{ $hadir['status_absensi'] }}</td>
							@elseif($hadir['status_absensi'] == 'C')
							<td><span class="label label-default label-mini">{{ $hadir['status_absensi'] }}</td>
							@elseif($hadir['status_absensi'] == 'W')
							<td><span class="label label-primary label-mini">{{ $hadir['status_absensi'] }}</td>
							@elseif($hadir['status_absensi'] == 'D')
							<td><span class="label label-info label-mini">{{ $hadir['status_absensi'] }}</td>
							@elseif($hadir['status_absensi'] == 'S')
							<td><span class="label label-default label-mini">{{ $hadir['status_absensi'] }}</td>
							@else
							<td>{{ $hadir['status_absensi'] }}</td>
							@endif

							@if($hadir['status_isian'] == 'OK' && $hadir['created_at']->format('Y-m-d') > $hadir['tanggal'])
							<td style="background-color:#f0ad4e; min-width:27px;" align="center	"><span class="label label-warning label-mini">{{ $hadir['status_isian'] }}</td>
							@elseif($hadir['status_isian'] == 'OK' && $hadir['created_at']->format('Y-m-d') == $hadir['tanggal'])
							<td style="background-color:#5cb85c; min-width:27px" align="center"><span class="label label-success label-mini">{{ $hadir['status_isian'] }}</td>
							@elseif($hadir['status_isian'] == 'C')
							<td><span class="label label-default">{{ $hadir['status_isian'] }}</td>
							@elseif($hadir['status_isian'] == 'S')
							<td><span class="label label-default">{{ $hadir['status_isian'] }}</td>
							@else
							<td style="font-weight:bold; color:red;">{{ $hadir['status_isian'] }}</td>
							@endif

							@else
							<td style="min-width:27px"></td>
							<td style="min-width:27px"></td>
							@endif

							@endforeach
						</tr>
						@endforeach
					</tbody>

				</table>
				@endif

				{{-- TAMPILAN TABLE USER --}}
				@if(\Auth::user()->role_id == 3)

				<table class="table">
					<thead>
						<tr>
							<th rowspan=2>Nama Pekerja</th>
							@foreach ($tanggal as $date)
							<th colspan=2 style="text-align:center">{{ \Carbon\Carbon::parse($date)->format('d') }}</th>
							@endforeach
						</tr>
						<tr>
							@foreach ($tanggal as $date)
							<th style="text-align:center">A</th>
							<th style="text-align:center">T</th>
							@endforeach
						</tr>
					</thead>
					<tbody>

						<tr>
							<td style="min-width:175px">{{ $user['nama'] }}</td>

							@foreach ($tanggal as $date)

							@if ($hadir = NULL) @endif  
							@if ($timesheet = \App\Timesheet::where('nip_pekerja', '=', $user['nip'])->where('tanggal', '<=', \Carbon\Carbon::now())->get()->sortBy('tanggal')) 
							@endif 

							@foreach($timesheet as $ts)
							@if($ts['tanggal'] == $date)
							@if ($hadir = $ts) @endif 
							@endif
							@endforeach

							@if($hadir != NULL)

							@if($hadir['status_absensi'] == 'H')
							<td align="center"><span class="label label-success label-mini">{{ $hadir['status_absensi'] }}</td>
							@elseif($hadir['status_absensi'] == 'C')
							<td><span class="label label-default label-mini">{{ $hadir['status_absensi'] }}</td>
							@elseif($hadir['status_absensi'] == 'W')
							<td><span class="label label-primary label-mini">{{ $hadir['status_absensi'] }}</td>
							@elseif($hadir['status_absensi'] == 'D')
							<td><span class="label label-info label-mini">{{ $hadir['status_absensi'] }}</td>
							@elseif($hadir['status_absensi'] == 'S')
							<td><span class="label label-default label-mini">{{ $hadir['status_absensi'] }}</td>
							@else
							<td>{{ $hadir['status_absensi'] }}</td>
							@endif

							@if($hadir['status_isian'] == 'OK' && $hadir['created_at']->format('Y-m-d') > $hadir['tanggal'])
							<td style="background-color:#f0ad4e; min-width:27px;" align="center	"><span class="label label-warning label-mini">{{ $hadir['status_isian'] }}</td>
							@elseif($hadir['status_isian'] == 'OK' && $hadir['created_at']->format('Y-m-d') == $hadir['tanggal'])
							<td style="background-color:#5cb85c; min-width:27px" align="center"><span class="label label-success label-mini">{{ $hadir['status_isian'] }}</td>
							@elseif($hadir['status_isian'] == 'C')
							<td><span class="label label-default">{{ $hadir['status_isian'] }}</td>
							@elseif($hadir['status_isian'] == 'S')
							<td><span class="label label-default">{{ $hadir['status_isian'] }}</td>
							@else
							<td style="font-weight:bold; color:red;">{{ $hadir['status_isian'] }}</td>
							@endif

							@else
							<td style="min-width:27px"></td>
							<td style="min-width:27px"></td>
							@endif

							@endforeach
						</tr>
					</tbody>
				</table>
				@endif

				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
	<div class="col-md-6">
		<strong>ABSENSI</strong>
		<table class='table table-condensed table-keterangan'>
			<tr>
				<td class="col-md-1 text-right"><span class="label label-success label-mini">H</span></td><td class="col-md-5">Hadir</td>
				<td class="col-md-1 text-right"><span class="label label-default label-mini">C</span></td><td class="col-md-5">Cuti</td>
			</tr>
			<tr>
				<td class="col-md-1 text-right"><span class="label label-info label-mini">D</span></td><td class="col-md-5">Dinas Luar</td>
				<td class="col-md-1 text-right"><span class="label label-default label-mini">S</span></td><td class="col-md-5">Sakit</td>
			</tr>
			<tr>
				<td class="col-md-1 text-right"><span class="label label-primary label-mini">W</span></td><td class="col-md-5">Work From Home</td>
				<td class="col-md-1 text-right"></td><td class="col-md-5"></td>
			</tr>


		</table>
	</div>

	<div class="col-md-6">
		<strong>TIMESHEET</strong>
		<table class='table table-condensed table-keterangan'>
			<tr>
				<td class="col-md-1 text-right"><span class="label label-success label-mini">OK</span></td><td class="col-md-5">Sudah Mengisi</td>
				<td class="col-md-1 text-right"><span class="label label-default label-mini">C</span></td><td class="col-md-5">Cuti</td>
				
			</tr>
			<tr>
				<td class="col-md-1 text-right"><span class="label label-warning label-mini">OK</span></td><td class="col-md-5">Terlambat Mengisi</td>
				<td class="col-md-1 text-right"><span class="label label-default label-mini">S</span></td><td class="col-md-5">Sakit</td>
			</tr>
		</table>
	</div>
</div>
<div>&nbsp;</div>


	

@stop

