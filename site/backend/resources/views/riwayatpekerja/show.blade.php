@extends('app')

@section('title')
Riwayat Pekerja - {{ $user->nama }}
@stop

@section('content')
{{-- hahahah --}}

@if(\Auth::user()->role_id == 1)
	<h1>> <a href="{{ action('RiwayatPekerjaController@index') }}">Riwayat Pekerja</a> > {{ $user->nama }}</h1>
@else
	<h1>> Riwayat Pekerja > {{ $user->nama }}</h1>
@endif
<br><br>
<div class="content-panel">
	<table class="table table-fixed table-striped">
		<tbody>
			<tr>
				<th class="text-right col-md-3 text-right">Nama:</th>
				<td class="col-md-9">{{ $user->nama }}</td>
			</tr>
			<tr>
				<th class="text-right col-md-3 text-right">Username:</th>
				<td class="col-md-9">{{ $user->username }}</td>
			</tr>
			<tr>
				<th class="text-right col-md-3 text-right">Jabatan:</th>
				<td class="col-md-9">{{ $user->jabatan }}</td>
			</tr>
			<tr>
				<th class="text-right">Divisi:</th>
				<td>
				@if($user->team != null)
						{{ $user->team->team_name }}
				@endif
				</td>
			</tr>
			<tr><th class="text-right">Projects:</th>
				<td>
					<ul class="list-unstyled">
						@foreach($user->project as $project)
						<li> {{ $project->project_name }} </li>
						@endforeach
					</ul>
				</td>
			</tr>
		</tbody>
	</table>
</div>



@stop

@section('footer')