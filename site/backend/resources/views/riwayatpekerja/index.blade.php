@extends('app')

@section('title')
	Riwayat Pekerja
@stop

@section('content')
	<h1>Riwayat Pekerja </h1>
	<hr>
	<div class="form-panel" >
	<table class="table table-hover">
		<thead>
		<tr>
			<th>Divisi</th><th>Nama Pekerja</th>
		</tr>
		</thead>
		<tbody>
			{{-- hahaha --}}
 		@foreach ($users as $user)
			<tr style="font-size:12pt;">
				<td>
					@if($user->team != null)
						{{ $user->team->team_name }}
					@endif
				</td>
				<td><a href="{{ action('RiwayatPekerjaController@show', $user->nip) }}">{{ $user->nama }}</a></td>
			</tr>
		@endforeach
		</tbody>
	</table>
	<center>{!! $users->render() !!}</center>
</div>
@stop

@section('script')
<script>
jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.document.location = $(this).data("href");
    });
});
</script>
@stop