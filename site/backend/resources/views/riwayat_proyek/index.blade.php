@extends('app')

@section('title')
    Riwayat Proyek
@stop

@section('content')
    <h1>Riwayat Proyek </h1>    
    <hr>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>ID proyek</th><th>Nama Proyek</th>
        </tr>
        </thead>
        <tbody>
        {{-- hahaha --}}
        @foreach ($riwayatproyek as $riwayatproject)
            <tr style="font-size:12pt;">
                <td>{{ $riwayatproject->project_id }}</td>
                <td><a href="{{ action('RiwayatProyekController@show', $riwayatproject->project_id) }}">{{ $riwayatproject->project_name}} </a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {!! $riwayatproyek->render() !!}
</div>
@stop

@section('script')
<script>
jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.document.location = $(this).data("href");
    });
});
</script>
@stop