@extends('app')

@section('title')
    Riwayat Proyek - {{ $project->project_name }}
@stop

@section('content')
    {{-- hahahah --}}
    <h1>> <a href="{{ action('RiwayatProyekController@index') }}">Riwayat Proyek</a> > {{ $project->project_name }}</h1>
    <br><br>
    <div class="content-panel" >    
    <div class="col-md-6 col-sm-offset-3">
        <div class="row">
        <div class="form-group col-sm-4">
            <label>Nama Proyek</label>
            <h2>{{ $project->project_name}}</h2>
        </div>

        <div class="form-group col-sm-8">
            <label>Leader</label>
            <h2>{{ $leader->nama }}</h2>
        </div>
        </div>

        <h3>Anggota</h3>
        <div class="table-responsive">
            <table class="table table-fixed table-responsive table-hover">
                <thead>
                    <tr>
                        <th>Divisi</th>
                        <th>Nama</th>
                    </tr>
                </thead>
                <tbody>



                       @foreach ($project->user as $user)
                           <tr>
                           <td align="center">
                            @if($user->team != null)
                                {{ $user->team->team_name }}
                            @else
                            --
                            @endif
                           </td>
                           <td align="center">{{$user->nama }}<td>
                           </tr>
                       @endforeach



                </tbody>

            </table>
        </div>
        
    </div>
    
</div>


    {{--<label>NIP	 :</label>{{ $user->nip }}<br>--}}

    {{--<label>Divisis  :</label>{{ $user->team_id }}<br>--}}
    {{--<label>Nama  :</label>{{ $user->username }}<br>--}}

    {{--<label>Email  :</label>{{ $user->email }}<br>--}}
    {{--<label>No. Telepon  :</label>{{ $user->no_telp }}<br>--}}
    {{--<label>Jabatan  :</label>{{ $user->jabatan }}<br>--}}
    {{--<label>Team  :</label>{{ $user->team->team_name }}<br>--}}
    {{--<label>Role  :</label>{{ $user->role->role_name }}<br>--}}


    {{--<a href="#" type="button" class="btn btn-primary">Edit</a>--}}
    {{--<a href="#" type="button" class="btn btn-danger">Hapus</a>--}}

@stop

@section('footer')