@extends('app')

@section('title')
Proyek - Tambah proyek baru
@stop


@section('content')
<h1>> <a href="{{ action('RiwayatProyekController@index') }}">Proyek</a> > Buat Proyek</h1>
<div class="form-panel">    
	{!! HTML::ul($errors->all()) !!}
    {!! Form::open(['url' => 'riwayatproyek', 'id' => 'createproyek']) !!}

    <div class="col-md-6 form-group">
        <h4>Project</h4>
        <hr>
        <div class="form-group form-inline">
            <div class="form-group">
                {!! Form::label('name', 'Nama Proyek') !!}<br>
                {!! Form::text('nama_proyek', Input::old('name'), array('class' => 'form-control')) !!}
                
            </div>

            <div class="form-group">
                {!! Form::label('team_id', 'Team Leader') !!}
                <br>
                {!! Form::select('team_id', $team_leaders, null, array('class' => 'form-control selectpicker')) !!}
            </div>           
        </div>                
    </div>

    <div class="col-md-6 form-group">
        <h4>Anggota</h4>
        <hr>               
        <div class="form-inline form-group">
        	Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet 
        </div>    
    </div>

    <div class="form-group form-inline text-center">
        <button type="submit" class="form-control btn btn-primary" id="submitButton"><i class="fa fa-check"></i> Simpan</button>
    </div>    
    {!! Form::close() !!}
</div>


@stop

@section('script')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script>
$(document).on("click", "#submitButton", function(e) {
    e.preventDefault();
    bootbox.confirm("Anda yakin ingin membuat akun proyek ini?", function(result) {
        if (result) {
            $('#createproyek').submit();
        }
    });
});
</script>
@stop


@section('footer')



