<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>@yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
    <!--external css-->
    <link href="{{ asset('/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="{{ asset('/css/zabuto_calendar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/gritter/css/jquery.gritter.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/lineicons/style.css') }}"> 
    <link href="{{ asset('/js/fullcalendar/bootstrap-fullcalendar.css') }}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/js/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link href="{{ asset('/js/fancybox/jquery.fancybox.css') }}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/jquery-ui.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/jquery-ui.structure.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/jquery-ui.theme.css') }}">
    
    <!-- Custom styles for this template -->
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/style-responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/table-responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/dataTables.bootstrap.css') }}" rel="stylesheet">


    <script src="{{ asset('/js/chart-master/Chart.js') }}"></script>
    

    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!--main content start-->
      <section id="main-content">
              @include ('flash::message')
              @yield('content')
      </section><!-- /MAIN CONTENT -->

   <!-- js placed at the end of the document so the pages load faster -->
    <script src="{{ asset('/js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-ui.js') }}"></script>

    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <script class="include" type="text/javascript" src="{{ asset('/js/jquery.dcjqaccordion.2.7.js') }}"></script>
    <script src="{{ asset('/js/jquery.scrollTo.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.nicescroll.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/js/jquery.sparkline.js') }}"></script>
    



    <!--common script for all pages-->
    <script src="{{ asset('/js/common-scripts.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/js/gritter/js/jquery.gritter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/gritter-conf.js') }}"></script>

    <!--script for this page-->
    <script src="{{ asset('/js/sparkline-chart.js') }}"></script>    
  <script src="{{ asset('/js/zabuto_calendar.js') }}"></script>
    <script src="{{ asset('/js/chart-master/Chart.js') }}"></script>
    <script src="{{ asset('/js/chartjs-conf.js') }}"></script>

  <!--custom switch-->
  <script src="{{ asset('/js/bootstrap-switch.js') }}"></script>
  
  <!--custom tagsinput-->
  <script src="{{ asset('/js/jquery.tagsinput.js') }}"></script>
  
  <!--custom checkbox & radio-->
  
  <script type="text/javascript" src="{{ asset('/js/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/js/bootstrap-daterangepicker/date.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  
  <script type="text/javascript" src="{{ asset('/js/bootstrap-inputmask/bootstrap-inputmask.min.js') }}"></script>


  
  
  <script src="{{ asset('/js/form-component.js') }}"></script>    


    
  
  </body>
</html>
