@extends('app')

@section('title')
Isi Form Lembur
@stop

@section('style')

<link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap-slider.css')}}">
@stop

@section('content')
	<h1>> <a href="{{ action('FormLemburController@index') }}">Form Lembur Saya</a> > Isi Form</h1>
<br><br>
	<div class="form-panel col-md-8">
	{!! Form::open(['url' => 'formlembur/create', 'id' => 'formlembur']) !!}
		<!-- Jenis Cuti Form Input -->
		<div class="form-group">
            <label>Pukul: </label>
            <input type="text" name="waktumulai" id="waktumulai" value="18:00" style="border:none; color: #666666; text-align:center" size="7" readonly/>
            <label>-</label>
			<input type="text" name="waktuselesai" id="waktuselesai" value="23:59" style="border:none; color: #666666; text-align:center" size="7" readonly/><br>
			<input type="text" class="slider" id="sl1" name="q12" value="" data-slider-min="1080" data-slider-max="1440" data-slider-step="5" data-slider-value="[1080,1440]" data-slider-orientation="horizontal" data-slider-selection="after" data-slider-tooltip="hide">			          
         </div>

        <div class="form-group {{ $errors->has('project') ? 'has-error' : '' }}">     
        	<label>Project :</label>        	
        	@if ($projects->isEmpty())
        		<p>No project assigned to you right now. Please <a href="pesan/create">contact</a> admin</p>
        	@else
        		<select class="form-control selectpicker" name="project">        	
        		@foreach ($projects as $project)
        			<option value="{{$project->project_id}}">{{$project_name}}</option>
        		@endforeach
        		</select>
        	@endif
        	{!! $errors->first('project', '<span class="help-block">:message</span>') !!}
        </div>

					
		<div class="form-group {{ $errors->has('keterangan') ? 'has-error' : '' }}">
			{!! Form::label('Keterangan', 'Keterangan:') !!}
			{!! Form::textarea('keterangan', null, ['class'=>'form-control']) !!}
			{!! $errors->first('keterangan', '<span class="help-block">:message</span>') !!}
		</div>
		<div class="form-group form-inline text-center">
			<button type="submit" class="form-control btn btn-primary btn-block" id="submitButton"><i class="fa fa-check"></i> Simpan</button>
		</div>
		</div>	
		<!-- Add Article Form Input hahaha-->
		
		
		
			
	{!! Form::close() !!}
	
	
@stop

@section('script')
		<script src="{{ asset('/js/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

	<script type="text/javascript">
    	$("#slider-range").slider({
		    range: true,
		    min: 1080,
		    max: 1440,
		    step: 5,
		    values: [1080, 1260],
		    slide: function (e, ui) {
		        var hours1 = Math.floor(ui.values[0] / 60);
		        var minutes1 = ui.values[0] - (hours1 * 60);

		        //diparsing ke string
		        hours1 = hours1.toString();
		        minutes1 = minutes1.toString();

		        //baru bisa diginiin, jadi format 09:00 dsb
		        if (hours1.length == 1) hours1 = '0' + hours1;
		        if (minutes1.length == 1) minutes1 = '0' + minutes1;
		        if (minutes1 == 0) minutes1 = '00';
		        /*if (hours1 >= 12) {
		            if (hours1 == 12) {
		                hours1 = hours1;
		                minutes1 = minutes1 + " PM";
		            } else {
		                hours1 = hours1 - 12;
		                minutes1 = minutes1 + " PM";
		            }
		        } else {
		            hours1 = hours1;
		            minutes1 = minutes1 + " AM";
		        }
		        if (hours1 == 0) {
		            hours1 = 12;
		            minutes1 = minutes1;
		        }*/



		        $('#waktumulai').val(hours1 + ':' + minutes1);

		        var hours2 = Math.floor(ui.values[1] / 60);
		        var minutes2 = ui.values[1] - (hours2 * 60);

		        hours2 = hours2.toString();
		        minutes2 = minutes2.toString();

		        if (hours2.length == 1) hours2 = '0' + hours2;
		        if (minutes2.length == 1) minutes2 = '0' + minutes2;
		        if (minutes2 == 0) minutes2 = '00';
		        if (hours2 == 24) {
		        	hours2 = 23;
		        	minutes2 = 59;
		        }

		        //INI KALO MAU MODE AM FM
		        /*if (hours2 >= 12) {
		            if (hours2 == 12) {
		                hours2 = hours2;
		                minutes2 = minutes2 + " PM";
		            } else if (hours2 == 24) {
		                hours2 = 11;
		                minutes2 = "59 PM";
		            } else {
		                hours2 = hours2 - 12;
		                minutes2 = minutes2 + " PM";
		            }
		        } else {
		            hours2 = hours2;
		            minutes2 = minutes2 + " AM";
		        }*/

		        $('#waktuselesai').val(hours2 + ':' + minutes2);
		    }
		});
    </script>
    <script src="{{ asset('/js/bootbox.min.js') }}"></script>
    <script>
        $(document).on("click", "#submitButton", function(e) {
        	e.preventDefault();
            bootbox.confirm("Anda yakin ingin menyimpan form ini?", function(result) {
                if (result) {
		            $('#formlembur').submit();
		        }
            });
        });
    </script>

    

@stop

