<head>
	<br><h1><u><center>FORM LEMBUR</center></u></h1></br>
</head>

<body>
<table align="center">
		<tbody align="left">
			<tr>
				<th><i>Employee (Nama Karyawan)</i></th> <th>:</th>
				<th>{{ \Auth::user()->nama }}</th>
			</tr>

			<tr>
				<th><i>Employee Number (NIP)</i></th> <th>:</th>
				<th><b>{{ \Auth::user()->nip }}</b></th>
			</tr>

			<tr>
				<th><i>Division (Divisi)</i></th> <th>:</th>
				<th><b>{{ \Auth::user()->team['team_name'] }}</b></th>
			</tr>

			<tr>
				<th><i>Section (Seksi)</i></th> <th>:</th>
				<th><b>{{ \Auth::user()->jabatan }}</b></th>
			</tr>
		</tbody>
</table>
<br></br>
<br></br>
<table id="example" border="1" align="center">
		 <tbody>
		 	<tr>
				<th>Created</th>
				<th>Begin</th>
				<th>Finish</th>
				<th>Duration</th>
				<th>Task</th>
				<th>Approval</th>
			</tr>

			<tr>
			
				<th>{{ $form->created_at }}</th>
				<th>{{ $form->formlembur->jam_mulai }}</th>
				<th>{{ $form->formlembur->jam_selesai }}</th>
				<th>{{ ($form->formlembur['jam_selesai'] - $form->formlembur['jam_mulai']) }}</th>
				<th>{{ $form->formlembur->keterangan }}</th>
				<th>{{ $form->status }}</th>
			</tr>
		</tbody>	 
	</table>
	</body>