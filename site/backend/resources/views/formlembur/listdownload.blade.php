@extends('app')

@section('title')
Form Lembur Saya
@stop


@section('content')
<h1>> Form Lembur</h1>
<div class="form-panel">
    <div class="text-center">
        {{-- Menampilkan notifikasi sukses --}}
        @if (Session::get('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
        @endif
    </div>

    <div class="form-group form-inline">
        <a href="{{ action('FormLemburController@create') }}" type="button" class="form-control btn btn-primary"><i class="fa fa-file"></i> Buat Form Lembur</a></td>
    @if(\Auth::user()->role_id=='2')
    <a href="{{ action('FormLemburController@showFormList') }}" type="button" class="form-control btn btn-default"><i class="fa fa-check"></i> Approve Form Lembur</a>
    @endif
    
</div>
    <table class="table table-hover">
        <thead>
           <tr>
              <th>Bulan</th><th>Jumlah Lembur</th><th>Action</th>
          </tr>
      </thead>
      <tbody>
        @foreach($bulan as $key => $month)
        <tr>
            <td>{{ $month }}</th>
                <td>{{ $jumlahform[$key] }}</th>
                    <td><a target='_blank' href="{{ action('FormLemburController@download', $key) }}" type="button" class="btn btn-primary btn-xs">Download</a></th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


        @stop