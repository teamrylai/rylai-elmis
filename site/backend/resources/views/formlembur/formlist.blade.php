@extends('app')

@section('title')
Approve Form Lembur
@stop


@section('content')
<h1>> Approve Form Lembur</h1>
<div class="form-panel">
    <div class="text-center">
        {{-- Menampilkan notifikasi sukses --}}
        @if (Session::get('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
        @endif
    </div>

    <div class="form-group form-inline">
        <a href="{{ action('FormLemburController@index') }}" type="button" class="form-control btn btn-secondary">Form Lembur Saya</a>
    </div>
    <div class="table-responsive form-group">
        <div class="table-outer">
            <div class="table-rekap">
                <table class="table table-fixed table-striped table-hover table-condensed">
                    <thead>
                     <tr>
                      <th>Tanggal</th><th>User</th><th>Durasi (Jam)</th><th>Keterangan</th><th>Status</th><th>Action</th>
                  </tr>
              </thead>
              <tbody>
                {{-- hahaha --}}
                @foreach ($forms as $form)
                <tr>
                    <td>{{ $form->created_at->toDateString() }}</td>
                    <td>{{ $form->user['nama'] }}</td>
                    <td>{{ ($form->formlembur->jam_selesai - $form->formlembur->jam_mulai) }}</td>
                    <td>{{ $form->formlembur->keterangan }}</td>
                    @if($form->status == 'approved')
                    <td><span class="label label-success label-mini">{{ $form->status }}</td>
                    @else
                    <td><span class="label label-warning label-mini">{{ $form->status }}</td>
                    @endif
                    <td>
                        @if($form->status=='pending')
                        <a href="{{ action('FormLemburController@approve',array($form->kode)) }}" type="button" class="btn btn-success btn-xs"><i class="fa fa-check"></i></a>
                        @endif
                        @if($form->status=='approved')
                        <a href="{{ action('FormLemburController@approve',array($form->kode)) }}" type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
@stop