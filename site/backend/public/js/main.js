$(document).ready(function () {
  var isClosed;

  runInit();

}); // end document.ready
$(window).load(function() {
  fileInput();
  selectpicker();
  slider();
  datepicker();
});
/**
 * Task yang dijalankan ketika document ready (page specific)
 * @return {[type]} [description]
 */
$(document).ready(function () {
}); // end document.ready

/**
 * Kumpulan task yang dijalankan ketika document ready
 * @return {[type]} [description]
 */
function runInit() {

  runInitAndResize();

  // on window resize event
  $(window).resize(function (){
      var delay;
      clearTimeout(delay);
      delay = setTimeout(runResizeTask(), 250);
  });
}

/**
 * Kumpulan task yang dijalankan ketika window diresize
 * @return {[type]} [description]
 */
function runResizeTask() {
  runInitAndResize();
}

/**
 * Kumpulan task yang dijalankan ketika document ready dan window diresize
 * @return {[type]} [description]
 */
function runInitAndResize() {
  setContentMinHeight();
}

function fileInput() {
  $('input[type=file]').bootstrapFileInput();
  $('.file-inputs').bootstrapFileInput();
  $('.file-input-name').text('No File Chosen');
}

function selectpicker() {
  $('.selectpicker').selectpicker();
}


function datepicker() {
  $('.date').datetimepicker();
}

function setContentMinHeight() {
  var navHeight = $('nav').height();
  var footerHeight = $('footer').height();
  var windowHeight = $(window).height();
  $('#content').css('min-height', windowHeight - (footerHeight + navHeight + 25) );
}

function slider() 
    {
      $(".slider").slider({
        formatter: function(value) {
        return 'Current value: ' + value;
      }
      });

      $(".slider").on("slide", function(slideEvt) {
          var hours1 = Math.floor(slideEvt.value[0] / 60);
          var minutes1 = slideEvt.value[0] - (hours1 * 60);

          hours1 = hours1.toString();
            minutes1 = minutes1.toString();
           
            if (hours1.length == 1) hours1 = '0' + hours1;
            if (minutes1.length == 1) minutes1 = '0' + minutes1;
            if (minutes1 == 0) minutes1 = '00';


            $('#waktumulai').val(hours1 + ':' + minutes1);        

        var hours2 = Math.floor(slideEvt.value[1] / 60);
            var minutes2 = slideEvt.value[1] - (hours2 * 60);

            hours2 = hours2.toString();
            minutes2 = minutes2.toString();

            if (hours2.length == 1) hours2 = '0' + hours2;
            if (minutes2.length == 1) minutes2 = '0' + minutes2;
            if (minutes2 == 0) minutes2 = '00';
            if (hours2 == 24) {
              hours2 = 23;
              minutes2 = 59;
            }

        $("#waktuselesai").val(hours2 + ':' + minutes2);

      });
  }