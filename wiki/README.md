## Description
WIKI folder is a place where you have to update for every activity you do before commiting to repository. 

### Folder Structure 
* a.txt
* b.txt
* c.txt
----

## IMPORTANT
Please keep files updated so that we can monitor changes easily.

for details on how to use bitbucket and contribute, please refer to : 
[bitbucket 101](https://confluence.atlassian.com/display/BITBUCKET/bitbucket+101)

### About commit message
Before commiting, please update wiki file related. 

When commit your code please use template :

	[feature-name] minim description 

	[description]
	
Or if it's so minim use :

	what things you've done

### Using Tags 

	TODO is tag for job need to be done or implemented later on
	HACK is tag for that representing your trials 
	XXX is just a damn flag.
	Please refer to Coding Standard for complete tags.

##HappyCode :D
