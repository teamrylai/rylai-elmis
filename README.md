## Description
Please fill in with project's description

## Features
The current application has this following features :

*  please fill in with project's features

### Package Structure 
* wiki
* app : for mobile app related code (iOS project, Android project)
* database : for database related files (mysql, sqlite, postgre, etc)
* design : for design related files (.psd, .jpg, .png)
* site : for web dev related code (.php, .html, .css, .js, etc)
----

for details on how to use bitbucket and contribute, please refer to : 
[bitbucket 101](https://confluence.atlassian.com/display/BITBUCKET/bitbucket+101)

### About commit message
Before commiting, please update wiki file related. 

When commit your code please use template :

	[feature-name] minim description 

	[description]
	
Or if it's so minim use :

	what things you've done

### Using Tags 

	TODO is tag for job need to be done or implemented later on
	HACK is tag for that representing your trials 
	XXX is just a damn flag.
	Please refer to Coding Standard for complete tags.

##HappyCode :D
